package com.tallink.complaintregister.unit.mapping

import com.tallink.complaintregister.AbstractMappingTest
import com.tallink.complaintregister.dao.model.domain.*

import java.time.ZonedDateTime

class ComplaintMappingTest extends AbstractMappingTest {

    def "Convert com.tallink.complaintregister.dao.model.domain.Complaint to com.tallink.complaintregister.web.api.Complaint"() {
        given:
        def source = new Complaint(
                complaintId: 1,
                issueDate: ZonedDateTime.now(),
                supplier: new Supplier(
                        supplierId: 1,
                        name: "supplier name",
                        address: "supplier address",
                        email: "supplier@tallink.ee",
                        registryNumber: "supplier registry number"
                ),
                location: new Location(
                        locationId: 1,
                        value: "location value"
                ),
                complaintStatus: new ComplaintStatus(
                        complaintStatusId: 1,
                        name: "complaint status name"
                ),
                user: new User(
                        userId: 1,
                        username: "user name",
                        userRole: new UserRole(
                                userRoleId: 1,
                                name: "user role name"
                        )
                ),
                complaintItems: []
        )
        when:
        def destination = customMapper.map(source, com.tallink.complaintregister.web.api.Complaint.class)
        then:
        source.complaintId == destination.complaintId
        source.issueDate == destination.issueDate
        and:
        source.supplier.name == destination.supplier.name
        source.supplier.email == destination.supplier.email
        source.supplier.address == destination.supplier.address
        source.supplier.registryNumber == destination.supplier.registryNumber
    }

}
