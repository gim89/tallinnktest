package com.tallink.complaintregister

import com.tallink.complaintregister.mapping.CustomMapper
import spock.lang.Shared
import spock.lang.Specification

abstract class AbstractMappingTest extends Specification {
    @Shared
    CustomMapper customMapper = new CustomMapper()
}