package com.tallink.complaintregister;

public class ServiceNames {
    public static final String LOCAL_DATABASE_DATA_SOURCE = "localDatabaseDataSource";
    public static final String LOCAL_DATABASE_TRANSACTION_MANAGER = "localDatabaseTransactionManager";
    public static final String LOCAL_DATABASE_ENTITY_MANAGER_FACTORY = "localDatabaseEntityManagerFactory";
    public static final String LOCAL_DATABASE_LIQUIBASE = "localDatabaseLiquibase";

    public static final String EMAIL_MARSHALLER = "complaintRegisterEmailMarshaller";
    public static final String CUSTOM_EMAIL_WEB_SERVICE_TEMPLATE = "complaintRegisterCustomEmailWebServiceTemplate";
}
