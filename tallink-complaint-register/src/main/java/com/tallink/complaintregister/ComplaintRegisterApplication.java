package com.tallink.complaintregister;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ComplaintRegisterApplication {

    public static void main(String[] args) {
        SpringApplication.run(ComplaintRegisterApplication.class, args);
    }

}
