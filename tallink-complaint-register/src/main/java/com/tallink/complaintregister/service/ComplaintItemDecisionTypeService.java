package com.tallink.complaintregister.service;

import com.tallink.complaintregister.dao.model.domain.ComplaintItemDecisionType;
import com.tallink.complaintregister.dao.repository.ComplaintItemDecisionTypeRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class ComplaintItemDecisionTypeService {

    @Autowired
    private ComplaintItemDecisionTypeRepository complaintItemDecisionTypeRepository;

    public Iterable<ComplaintItemDecisionType> findAll() {
        return complaintItemDecisionTypeRepository.findAll();
    }
}
