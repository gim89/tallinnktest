package com.tallink.complaintregister.service;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFFormulaEvaluator;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.springframework.stereotype.Service;

import com.tallink.complaintregister.web.api.Complaint;
import com.tallink.complaintregister.web.api.ComplaintItem;

import lombok.extern.slf4j.Slf4j;

import java.time.format.DateTimeFormatter;

/**
 * Created by alekspo on 9/16/2019.
 */
@Slf4j
@Service
public class ExcelGenerationService {
	public HSSFWorkbook generateExcelForComplaint(Complaint complaint) {
		HSSFWorkbook workbook = new HSSFWorkbook();
		HSSFSheet sheet = workbook.createSheet(String.format("Complaint_%s",complaint.getLocation().getValue()));

		HSSFFont propertyFont = workbook.createFont();
		HSSFFont valueFont = workbook.createFont();
		HSSFFont tableRowFont = workbook.createFont();
		propertyFont.setBold(true);
		propertyFont.setColor(HSSFColor.HSSFColorPredefined.BLACK.getIndex());
		valueFont.setBold(true);
		tableRowFont.setItalic(true);

		generateSupplierInfo(complaint, sheet, propertyFont, valueFont);
		generateBuyerInfo(workbook, sheet, propertyFont);

		HSSFCellStyle tableRowStyle = workbook.createCellStyle();
		tableRowStyle.setFont(tableRowFont);
		int currentWorkingRow = 11;
		for (ComplaintItem complaintItem : complaint.getComplaintItems()) {
			HSSFRow complaintItemRow = sheet.createRow(currentWorkingRow);
			{
				String cellFormula = "E" + (currentWorkingRow + 1) + "*" + "G"  + (currentWorkingRow + 1);
				complaintItemRow.createCell(0).setCellValue(
						(complaintItem.getProduct() != null && complaintItem.getProduct().getArticle() != null)
								? complaintItem.getProduct().getArticle() : ""
				);
				complaintItemRow.createCell(1).setCellValue(
						(complaintItem.getName() != null) ? complaintItem.getName() : ""
				);
				complaintItemRow.createCell(2).setCellValue(
						(complaintItem.getBestBefore() != null) ? complaintItem.getBestBefore().toLocalDate().toString() : ""
				);
				complaintItemRow.createCell(3).setCellValue(
						(complaintItem.getReceivingDate() != null) ? complaintItem.getReceivingDate().toLocalDate().toString() : ""
				);
				complaintItemRow.createCell(4).setCellValue(complaintItem.getAmount());
				complaintItemRow.createCell(5).setCellValue(
						(complaintItem.getProduct() != null && complaintItem.getProduct().getAbbreviation() != null)
								? complaintItem.getProduct().getAbbreviation() : ""
				);
				complaintItemRow.createCell(6).setCellValue(complaintItem.getPrice());
				complaintItemRow.createCell(7).setCellFormula(cellFormula);
				complaintItemRow.createCell(8).setCellValue(
						(complaintItem.getComplaintItemType() != null && complaintItem.getComplaintItemType().getName() != null)
								? complaintItem.getComplaintItemType().getName() : ""
				);
				complaintItemRow.createCell(9).setCellValue(
						(complaintItem.getComplaintItemReasonType() != null && complaintItem.getComplaintItemReasonType().getName() != null)
								? complaintItem.getComplaintItemReasonType().getName() : ""
				);
				complaintItemRow.createCell(10).setCellValue(
						(complaintItem.getComplaintItemDecisionType() != null && complaintItem.getComplaintItemDecisionType().getName() != null)
								? complaintItem.getComplaintItemDecisionType().getName() : ""
				);
				complaintItemRow.createCell(11).setCellValue(
						(complaintItem.getComment() != null) ? complaintItem.getComment() : ""
				);

				for (int i = 0; i < 12; i++) {
					complaintItemRow.getCell(i).setCellStyle(tableRowStyle);
				}
			}
			currentWorkingRow++;
		}

		currentWorkingRow++; // skip 1 empty row
		HSSFRow sumRow = sheet.createRow(currentWorkingRow);
		sumRow.createCell(6).setCellValue("Sum");
		sumRow.createCell(7).setCellFormula("SUM(H12:H" + (currentWorkingRow - 1) + ")");

		/*HSSFRow kmRow = sheet.createRow(++currentWorkingRow);
		kmRow.createCell(6).setCellValue("KM. 20%");
		kmRow.createCell(7).setCellFormula("H" + (currentWorkingRow) + "*0.2");*/

		HSSFRow totalRow = sheet.createRow(++currentWorkingRow);
		totalRow.createCell(6).setCellValue("Total");
		totalRow.createCell(7).setCellFormula("H" + (currentWorkingRow) + "+" + "H" + (currentWorkingRow - 1));

		currentWorkingRow += 2;
		sheet.createRow(currentWorkingRow).createCell(0).setCellValue("NB!!  The complaint is the basis for issuing a credit note. Please issue your credit note within 3 business days.");
		currentWorkingRow += 4;
		HSSFRow signRow = sheet.createRow(currentWorkingRow);
		signRow.createCell(0).setCellValue("…………………………..");
		signRow.createCell(5).setCellValue("…………………………..");
		HSSFRow signDescrRow = sheet.createRow(++currentWorkingRow);
		signDescrRow.createCell(0).setCellValue("Seller");
		signDescrRow.createCell(5).setCellValue("Buyer");

		HSSFFormulaEvaluator.evaluateAllFormulaCells(workbook);

		return workbook;
	}

	private void generateBuyerInfo(HSSFWorkbook workbook, HSSFSheet sheet, HSSFFont propertyFont) {
		HSSFRow tableHeadersRow = sheet.createRow(10);
		HSSFCellStyle tableHeadersStyle = workbook.createCellStyle();
		tableHeadersStyle.setFont(propertyFont);
		tableHeadersRow.createCell(0).setCellValue("Article code");
		tableHeadersRow.createCell(1).setCellValue("Product name");
		tableHeadersRow.createCell(2).setCellValue("Best before");
		tableHeadersRow.createCell(3).setCellValue("Reciving date");
		tableHeadersRow.createCell(4).setCellValue("Amount");
		tableHeadersRow.createCell(5).setCellValue("Unit");
		tableHeadersRow.createCell(6).setCellValue("Price");
		tableHeadersRow.createCell(7).setCellValue("Sum");
		tableHeadersRow.createCell(8).setCellValue("Type");
		tableHeadersRow.createCell(9).setCellValue("Reason");
		tableHeadersRow.createCell(10).setCellValue("Decision");
		tableHeadersRow.createCell(11).setCellValue("Comment");

		for (int i = 0; i < 12; i++) {
			tableHeadersRow.getCell(i).setCellStyle(tableHeadersStyle);
		}
	}

	private void generateSupplierInfo(Complaint complaint, HSSFSheet sheet, HSSFFont propertyFont, HSSFFont valueFont) {
		DateTimeFormatter defaultDateformat = DateTimeFormatter.ofPattern("dd.MM.yyyy");
		HSSFRow row1 = sheet.createRow(2);
		row1.createCell(0)
				.setCellValue(getHssfRichTextStringForData(propertyFont, valueFont, "Reference to invoice no: ", complaint.getComplaintId().toString()));
		row1.createCell(6).setCellValue("Buyer data:");
		row1.createCell(7)
				.setCellValue(getHssfRichTextStringForData(propertyFont, valueFont, "Complaint no: ", complaint.getReferenceNumber()));
		HSSFRow row2 = sheet.createRow(3);
		row2.createCell(0)
				.setCellValue(getHssfRichTextStringForData(propertyFont, valueFont, "Invoice date: ", complaint.getInvoiceDate().format(defaultDateformat).toString()));
		row2.createCell(7)
				.setCellValue(getHssfRichTextStringForData(propertyFont, valueFont, "Complaint issue date: ", complaint.getIssueDate().toLocalDate().toString()));
		sheet.createRow(4).createCell(7)
				.setCellValue(getHssfRichTextStringForData(propertyFont, valueFont, "Complaint author: ", complaint.getLocation().getValue()));
		HSSFRow row3 = sheet.createRow(5);
		row3.createCell(0)
				.setCellValue(getHssfRichTextStringForData(propertyFont, valueFont, "Seller: ", complaint.getSupplier().getName()));
		row3.createCell(7)
				.setCellValue(getHssfRichTextStringForData(propertyFont, valueFont, "Buyer: ", complaint.getBuyer().getName()));
		HSSFRow row4 = sheet.createRow(6);
		row4.createCell(0)
				.setCellValue(getHssfRichTextStringForData(propertyFont, valueFont, "Registry no: ", complaint.getSupplier().getRegistryNumber()));
		row4.createCell(7)
				.setCellValue(getHssfRichTextStringForData(propertyFont, valueFont, "Registry no: ", complaint.getBuyer().getRegistryNumber()));
		HSSFRow row5 = sheet.createRow(7);
		row5.createCell(0)
				.setCellValue(getHssfRichTextStringForData(propertyFont, valueFont, "Address: ", complaint.getSupplier().getAddress()));
		row5.createCell(7)
				.setCellValue(getHssfRichTextStringForData(propertyFont, valueFont, "Address: ", complaint.getBuyer().getAddress()));
	}

	private HSSFRichTextString getHssfRichTextStringForData(HSSFFont propertyFont, HSSFFont valueFont, String property, String value) {
		HSSFRichTextString arveNr = new HSSFRichTextString(property + value);
		arveNr.applyFont(0, property.length(), propertyFont);
		arveNr.applyFont(property.length() , property.length() + value.length(), valueFont);
		return arveNr;
	}
}
