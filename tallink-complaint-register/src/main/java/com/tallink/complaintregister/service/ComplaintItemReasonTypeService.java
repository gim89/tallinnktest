package com.tallink.complaintregister.service;

import com.tallink.complaintregister.dao.model.domain.ComplaintItemReasonType;
import com.tallink.complaintregister.dao.repository.ComplaintItemReasonTypeRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class ComplaintItemReasonTypeService {

    @Autowired
    private ComplaintItemReasonTypeRepository complaintItemReasonTypeRepository;

    public Iterable<ComplaintItemReasonType> findAll(){
        return complaintItemReasonTypeRepository.findAll();
    }
}
