package com.tallink.complaintregister.service;

import com.tallink.complaintregister.dao.model.domain.*;
import com.tallink.complaintregister.dao.model.domain.flat.Supplier;
import com.tallink.complaintregister.dao.repository.ComplaintRepository;
import com.tallink.complaintregister.dao.repository.ComplaintStatusRepository;
import com.tallink.complaintregister.dao.repository.UserRepository;
import com.tallink.complaintregister.mapping.CustomMapper;
import com.tallink.complaintregister.service.flat.*;
import com.tallink.complaintregister.web.api.ComplaintItem;
import com.tallink.complaintregister.web.api.auth.LoginResponse;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

@Slf4j
@Service
public class ComplaintService {

    @Autowired
    private ComplaintRepository complaintRepository;
	@Autowired
	private ComplaintStatusRepository complaintStatusRepository;
	@Autowired
	private ComplaintFlatService complaintFlatService;
	@Autowired
	private ComplaintItemFlatService complaintItemFlatService;
	@Autowired
	private LocationFlatService locationFlatService;
	@Autowired
	private SupplierFlatService supplierFlatService;
	@Autowired
	private ComplaintStatusFlatService complaintStatusFlatService;
	@Autowired
	private UserFlatService userFlatService;
	@Autowired
	private BuyerFlatService buyerFlatService;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private CustomMapper customMapper;
	@Autowired
	private AuthenticationService authenticationService;
	@Autowired
	private EmailService emailService;

	@Value("${hq.email.address}")
	private String headquartersEmail;

	@Value("${frontend.url}")
	private String frontendUrl;

    public Iterable<Complaint> findAllComplaints(){
        return complaintRepository.findAll();
    }

    public Complaint findComplaintByIdOrNull(Integer id) {
        Complaint complaintOptional = null;

        try {
            complaintOptional = complaintRepository.findById(id).get();
        } catch (Exception ex) {
            LOGGER.error("While trying to find complaint using id [" + id + "]", ex);
        }

        return complaintOptional;
    }

    public Iterable<Complaint> findComplaintWithStatus(String status) {
        Iterable<Complaint> complaintOptional = null;

        try {
        	if (status.equalsIgnoreCase("draft")){
				LoginResponse userDetails = authenticationService.getAuthenticatedUserDetailsOrNull();
				complaintOptional = complaintRepository.findComplaintByComplaintStatusNameAndUserUsername(status, userDetails.getUsername());
			}
        	else{
				complaintOptional = complaintRepository.findComplaintByComplaintStatusName(status);
			}
        } catch (Exception ex) {
            LOGGER.error("While trying to find complaint using id [" + status + "]", ex);
        }
        return complaintOptional;
    }

    public com.tallink.complaintregister.web.api.Complaint createComplaint(com.tallink.complaintregister.web.api.Complaint complaintWebApi) {
    	Complaint complaint = customMapper.map(complaintWebApi, com.tallink.complaintregister.dao.model.domain.Complaint.class);

		try {
			complaint.setSupplier(customMapper.map(
					saveOrUpdateSupplier(customMapper.map(complaint.getSupplier(), com.tallink.complaintregister.web.api.Supplier.class)),
					com.tallink.complaintregister.dao.model.domain.Supplier.class
					)
			);

			var user = authenticationService.getAuthenticatedUserDetailsOrNull();
			complaint.setLocation(findLocationOrNull(user.getLocation()));
			complaint.setComplaintStatus(findComplaintStatusOrNull(complaint.getComplaintStatus()));

			complaint.setUser(findUserOrNull(user));
			complaint.setBuyer(findBuyerByNameAndRegistryNumberOrNull(
					complaint.getBuyer().getName(),
					complaint.getBuyer().getRegistryNumber()
			));

			var savedComplaintFlat = complaintFlatService.save(
					customMapper.map(complaint, com.tallink.complaintregister.dao.model.domain.flat.Complaint.class)
			);
			complaint.getComplaintItems().forEach(complaintItem -> {
                var savedComplaintItemFlat = complaintItemFlatService.save(customMapper.map(complaintItem, com.tallink.complaintregister.dao.model.domain.flat.ComplaintItem.class));
                savedComplaintItemFlat.setComplaintId(savedComplaintFlat.getComplaintId());
                savedComplaintItemFlat.setComplaintItemTypeId(complaintItem.getComplaintItemType().getComplaintItemTypeId());
                savedComplaintItemFlat.setComplaintItemReasonTypeId(complaintItem.getComplaintItemReasonType().getComplaintItemReasonTypeId());
                savedComplaintItemFlat.setComplaintItemDecisionTypeId(complaintItem.getComplaintItemDecisionType().getComplaintItemDecisionTypeId());
                savedComplaintItemFlat.setProductId(complaintItem.getProduct().getProductId());
				complaintItemFlatService.save(savedComplaintItemFlat);
			});
			complaint.setComplaintId(savedComplaintFlat.getComplaintId());
		} catch (Exception ex) {
			LOGGER.error("While trying to create complaint: ", ex);
		}

		return customMapper.map(complaint, com.tallink.complaintregister.web.api.Complaint.class);
	}

	private Buyer findBuyerByNameAndRegistryNumberOrNull(String name, String registryNumber) {
    	com.tallink.complaintregister.dao.model.domain.flat.Buyer buyerFlat = null;
		try {
			buyerFlat = buyerFlatService.findBuyerByNameAndRegistryNumberOrNull(name, registryNumber);
		} catch (Exception ex) {
			LOGGER.error("While trying to find buyer using name [" + name + "] and registryNumber [" + registryNumber + "]", ex);
		}

		return customMapper.map(buyerFlat, com.tallink.complaintregister.dao.model.domain.Buyer.class);
	}

	public ComplaintStatus updateComplaintStatusOrNull(Integer complaintId, String status) {
		ComplaintStatus complaintStatusOptional = null;
		Complaint complaintOptional = null;

		try {
			complaintStatusOptional = complaintStatusRepository.findComplaintStatusByName(status);
		} catch (Exception ex) {
			LOGGER.error("While trying to find complaint using id [" + status + "]", ex);
		}

		if (complaintStatusOptional != null && complaintStatusOptional.getName() != null) {
			try {
				complaintOptional = complaintRepository.findById(complaintId).get();
			} catch (Exception ex) {
				LOGGER.error("While trying to find complaint using id [" + complaintId + "]", ex);
			}
			complaintOptional.setComplaintStatus(complaintStatusOptional);
			complaintRepository.save(complaintOptional);
		}

		return complaintStatusOptional;
	}

	public ComplaintStatus updateComplaintDao(
			Integer complaintId,
			com.tallink.complaintregister.dao.model.domain.Complaint complaint
	) {
		ComplaintStatus complaintStatusOptional = null;
		Complaint complaintOptional = null;
		String status = complaint.getComplaintStatus().getName();

		try {
			complaintStatusOptional = complaintStatusRepository.findComplaintStatusByName(status);
		} catch (Exception ex) {
			LOGGER.error("While trying to find complaint using id [" + status + "]", ex);
		}

		if (complaintStatusOptional != null && complaintStatusOptional.getName() != null) {
			try {
				complaintOptional = complaintRepository.findById(complaintId).get();
			} catch (Exception ex) {
				LOGGER.error("While trying to find complaint using id [" + complaintId + "]", ex);
			}
			complaintOptional.setComplaintStatus(complaintStatusOptional);
			complaintOptional.setInvoiceDate(complaint.getInvoiceDate());
			complaintOptional.setReferenceNumber(complaint.getReferenceNumber());
			complaintOptional.setIssueDate(complaint.getIssueDate());
			complaintOptional.setSupplier(updateComplaintDaoSupplier(complaint.getSupplier()));
			complaintRepository.save(complaintOptional);
		}

		return complaintStatusOptional;
	}

	private com.tallink.complaintregister.dao.model.domain.Supplier updateComplaintDaoSupplier(com.tallink.complaintregister.dao.model.domain.Supplier supplier) {
		com.tallink.complaintregister.dao.model.domain.flat.Supplier supplierFlat = supplierFlatService.findSupplierFlatByNameAndRegistryNumberOrNull(
				supplier.getName(),
				supplier.getRegistryNumber()
		);

		if (supplierFlat == null
				|| !supplier.getSupplierId().equals(supplierFlat.getSupplierId())
		) {
			return customMapper.map(supplierFlat, com.tallink.complaintregister.dao.model.domain.Supplier.class);
		}
		return supplier;
	}

	public com.tallink.complaintregister.web.api.Complaint updateComplaint(com.tallink.complaintregister.web.api.Complaint complaint) {
		saveOrUpdateSupplier(complaint.getSupplier());
		updateComplaintDao(complaint.getComplaintId(), customMapper.map(complaint, com.tallink.complaintregister.dao.model.domain.Complaint.class));
		saveComplaintItems(complaint.getComplaintId(), complaint.getComplaintItems());
		askHelpFromHeadQuarters(complaint);
		return complaint;
	}

	private void askHelpFromHeadQuarters(com.tallink.complaintregister.web.api.Complaint complaint) {

    	boolean isHelpNeededFromHeadquarters;

		isHelpNeededFromHeadquarters = complaint.getComplaintItems()
				.stream()
				.anyMatch(complaintItem -> complaintItem.getIsHelpNeeded().equals(true));

    	if (
			complaint.getLocation() != null
			&& complaint.getLocation().getValue() != null
			&& complaint.getComplaintId() != null
			&& complaint.getComplaintStatus().getName().equals("draft")
			&& isHelpNeededFromHeadquarters
		) {

			emailService.sendNotificationEmailToHeadquarters(
					headquartersEmail,
					"Please help with complaint",
					String.format(
							"<h2>Hello!</h2>" +
									"<p>Complaint number %s needs some help.</p>" +
									"<p>Please checkout the problem here: <a href=\"%s\">Here</a></p>",
							complaint.getLocation().getValue() + '-' + complaint.getComplaintId(),
							getComplaintDetailUrl(complaint)
							)
			);
		}
	}

	public String getComplaintDetailUrl(com.tallink.complaintregister.web.api.Complaint complaint) {
		return frontendUrl + "/complaint-detail/" + complaint.getComplaintId().toString();
	}

	private void saveComplaintItems(Integer complaintId, Set<ComplaintItem> complaintItems) {

		updateExistingComplaintItems(complaintId, complaintItems);

		createNewComplaintItems(complaintId, complaintItems);

		deleteExistingComplaintItems(complaintId, complaintItems);
	}

	private void deleteExistingComplaintItems(Integer complaintId, Set<ComplaintItem> complaintItems) {
		List<com.tallink.complaintregister.dao.model.domain.flat.ComplaintItem> complaintItemFlatList =
				complaintItemFlatService.findComplaintItemsByComplaintId(complaintId);

		List<com.tallink.complaintregister.dao.model.domain.flat.ComplaintItem> deletedComplaintItemFlatList
				= new ArrayList<>();

		complaintItemFlatList.forEach(itemFlat -> {

			if (complaintItems.stream().noneMatch(x -> x.getComplaintItemId().equals(itemFlat.getComplaintItemId()))) {
				deletedComplaintItemFlatList.add(itemFlat);
			}
		});

		deletedComplaintItemFlatList.forEach(itemFlat -> {
			if (itemFlat.getComplaintItemId() != null) {
				complaintItemFlatService.deleteByComplaintItemId(itemFlat.getComplaintItemId());
			}
		});
	}

	private void createNewComplaintItems(Integer complaintId, Set<ComplaintItem> complaintItems) {
		complaintItems.forEach(itemWeb -> {
			if ( itemWeb.getComplaintItemId() == null ) {
				com.tallink.complaintregister.dao.model.domain.flat.ComplaintItem itemFlat =
						customMapper.map(itemWeb, com.tallink.complaintregister.dao.model.domain.flat.ComplaintItem.class);
				itemFlat.setComplaintId(complaintId);
				complaintItemFlatService.save(itemFlat);
			}
		});
	}

	private void updateExistingComplaintItems(Integer complaintId, Set<ComplaintItem> complaintItems) {
		List<com.tallink.complaintregister.dao.model.domain.flat.ComplaintItem> complaintItemFlatList =
				complaintItemFlatService.findComplaintItemsByComplaintId(complaintId);

		complaintItems.forEach(itemWeb -> {
			complaintItemFlatList.forEach(itemFlat -> {
				if (
						itemWeb.getComplaintItemId() != null &&
						itemWeb.getComplaintItemId().equals(itemFlat.getComplaintItemId())
				) {
					customMapper.map(itemWeb, itemFlat);
					complaintItemFlatService.save(itemFlat);
				}
			});
		});
	}

	private Supplier saveOrUpdateSupplier(com.tallink.complaintregister.web.api.Supplier supplier) {

		com.tallink.complaintregister.dao.model.domain.flat.Supplier supplierFlat = supplierFlatService.findSupplierFlatByNameAndRegistryNumberOrNull(
				supplier.getName(),
				supplier.getRegistryNumber()
		);

		if (supplierFlat == null) {
			supplierFlat = supplierFlatService.save(customMapper.map(supplier, com.tallink.complaintregister.dao.model.domain.flat.Supplier.class));
		}

		if (
                ( StringUtils.isBlank(supplierFlat.getEmail()) && !StringUtils.isBlank(supplier.getEmail()) )
                || ( StringUtils.isBlank(supplierFlat.getAddress()) && !StringUtils.isBlank(supplier.getAddress()) )
        ) {
            supplierFlat.setEmail(supplier.getEmail());
            supplierFlat.setAddress(supplier.getAddress());
        }

		customMapper.map(supplierFlat, com.tallink.complaintregister.dao.model.domain.Supplier.class);
		return supplierFlatService.save(supplierFlat);
	}

	private Location findLocationOrNull(String location) {
		com.tallink.complaintregister.dao.model.domain.flat.Location locationFlat =
				locationFlatService.findByValue(location);

		if (locationFlat == null)
			return null;

		return customMapper.map(locationFlat, com.tallink.complaintregister.dao.model.domain.Location.class);
	}

	private User findUserOrNull(LoginResponse user) {
		com.tallink.complaintregister.dao.model.domain.flat.User userFlat =
				userFlatService.findByUsername(user.getUsername());

		if (userFlat == null)
			return null;

		return customMapper.map(userFlat, com.tallink.complaintregister.dao.model.domain.User.class);
	}

	private ComplaintStatus findComplaintStatusOrNull(ComplaintStatus complaintStatus) {
		com.tallink.complaintregister.dao.model.domain.flat.ComplaintStatus complaintStatusFlat =
				complaintStatusFlatService.findByName(complaintStatus.getName());

		if (complaintStatusFlat == null)
			return null;

		return customMapper.map(complaintStatusFlat, com.tallink.complaintregister.dao.model.domain.ComplaintStatus.class);
	}
}
