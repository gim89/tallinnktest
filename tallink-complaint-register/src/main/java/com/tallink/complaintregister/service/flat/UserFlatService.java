package com.tallink.complaintregister.service.flat;

import com.tallink.complaintregister.dao.model.domain.flat.User;
import com.tallink.complaintregister.dao.repository.flat.UserFlatRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class UserFlatService {

    @Autowired
    private UserFlatRepository userFlatRepository;

    public User findByUsername(String name) {

        return userFlatRepository.findByUsername(name);
    }
}
