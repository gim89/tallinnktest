package com.tallink.complaintregister.service;

import com.tallink.complaintregister.dao.model.domain.UserRole;
import com.tallink.complaintregister.dao.repository.UserRoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserRoleService {

    @Autowired
    private UserRoleRepository userRoleRepository;

    public Iterable<UserRole> findAllUserRoles(){
        return userRoleRepository.findAll();
    }
}
