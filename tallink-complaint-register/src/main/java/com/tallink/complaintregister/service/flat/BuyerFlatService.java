package com.tallink.complaintregister.service.flat;

import com.tallink.complaintregister.dao.model.domain.flat.Buyer;
import com.tallink.complaintregister.dao.repository.flat.BuyerFlatRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class BuyerFlatService {

    @Autowired
    private BuyerFlatRepository buyerFlatRepository;

    public Buyer findBuyerByNameAndRegistryNumberOrNull(String name, String registryNumber) {

        return buyerFlatRepository.findByNameAndRegistryNumber(name, registryNumber);
    }
}
