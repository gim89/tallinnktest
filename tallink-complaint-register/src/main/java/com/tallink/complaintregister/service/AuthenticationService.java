package com.tallink.complaintregister.service;

import com.tallink.complaintregister.config.security.SecurityUserDetails;
import com.tallink.complaintregister.dao.model.domain.User;
import com.tallink.complaintregister.dao.repository.UserRepository;
import com.tallink.complaintregister.web.api.auth.LoginResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class AuthenticationService {

	@Autowired
	private UserRepository userRepository;

	public LoginResponse getAuthenticatedUserDetailsOrNull() {
		var securityContext = SecurityContextHolder.getContext();
		if (securityContext == null) return null;
		else if (securityContext.getAuthentication() == null) return null;

		return (LoginResponse)securityContext.getAuthentication().getPrincipal();
	}

	public SecurityUserDetails findByName(String name){
		User user = userRepository.findUserByUsernameIgnoreCase(name);

		return new SecurityUserDetails(user);
	}
}
