package com.tallink.complaintregister.service;

import com.tallink.complaintregister.dao.model.domain.Product;
import com.tallink.complaintregister.dao.repository.ProductRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Iterator;

@Slf4j
@Service
public class ProductService {

    @Autowired
    private ProductRepository productRepository;

    public Iterable<Product> findAllProducts() {
        Iterable<Product> products = productRepository.findAll();

        Iterator<Product> productIterator = products.iterator();
        while (productIterator.hasNext()) {
            Product product = productIterator.next();
            if(product.getDesignation().toLowerCase().equals("empty")) {
                productIterator.remove();
            }
        }
        return products;
    }
}
