package com.tallink.complaintregister.service;

import com.tallink.complaintregister.dao.model.domain.Supplier;
import com.tallink.complaintregister.dao.repository.SupplierRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SupplierService {

    @Autowired
    private SupplierRepository supplierRepository;

    public Iterable<Supplier> findAll(){
        return supplierRepository.findAll();
    }
}
