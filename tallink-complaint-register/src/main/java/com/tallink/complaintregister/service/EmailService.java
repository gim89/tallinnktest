package com.tallink.complaintregister.service;

import com.tallink.complaintregister.email.ws.api.Attachment;
import com.tallink.complaintregister.email.ws.api.ObjectFactory;
import com.tallink.complaintregister.email.ws.api.SendEmailHTML;
import com.tallink.complaintregister.email.ws.api.SendEmailWithNamedAttachments;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.soap.client.core.SoapActionCallback;

import static com.tallink.complaintregister.ServiceNames.CUSTOM_EMAIL_WEB_SERVICE_TEMPLATE;

@Slf4j
@Service
public class EmailService {

	@Autowired
	@Qualifier(CUSTOM_EMAIL_WEB_SERVICE_TEMPLATE)
	private WebServiceTemplate customEmailWebServiceTemplate;

	@Value("${sent.email.address}")
	private String sentEmailAddress;

	public void sendEmailWithAttachment(String recipients, String subject, String body, String filename, byte[] document) {
		ObjectFactory emailOf = new ObjectFactory();
		SendEmailWithNamedAttachments emailWithNamedAttachment = emailOf.createSendEmailWithNamedAttachments();

		emailWithNamedAttachment.setAttachments(emailOf.createArrayOfAttachment());
		Attachment attachment = emailOf.createAttachment();
		attachment.setFileName(filename);
		attachment.setData(document);
		emailWithNamedAttachment.getAttachments().getAttachment().add(attachment);

		emailWithNamedAttachment.setFromAddress(sentEmailAddress);
		emailWithNamedAttachment.setToAddress(recipients);
		emailWithNamedAttachment.setSubject(subject);
		emailWithNamedAttachment.setMessageBody(body);

		customEmailWebServiceTemplate.marshalSendAndReceive(emailWithNamedAttachment, new SoapActionCallback(
				"http://tempuri.org/SendEmailWithNamedAttachments"));
	}

	public void sendNotificationEmailToHeadquarters(String recipients, String subject, String body) {
		ObjectFactory emailOf = new ObjectFactory();
		SendEmailHTML email = emailOf.createSendEmailHTML();

		email.setFromAddress("DoNotReply@tallink.ee");
		email.setToAddress(recipients);
		email.setSubject(subject);
		email.setMessageBody(body);

		customEmailWebServiceTemplate.marshalSendAndReceive(email);
	}
}
