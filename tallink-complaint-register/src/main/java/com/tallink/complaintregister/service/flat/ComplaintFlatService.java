package com.tallink.complaintregister.service.flat;

import com.tallink.complaintregister.dao.model.domain.flat.Complaint;
import com.tallink.complaintregister.dao.repository.flat.ComplaintFlatRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class ComplaintFlatService {

    @Autowired
    private ComplaintFlatRepository complaintFlatRepository;

    public Complaint save(Complaint complaintFlat) {
        return complaintFlatRepository.save(complaintFlat);
    }
}
