package com.tallink.complaintregister.service.flat;

import com.tallink.complaintregister.dao.model.domain.flat.ComplaintStatus;
import com.tallink.complaintregister.dao.repository.flat.ComplaintStatusFlatRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class ComplaintStatusFlatService {

    @Autowired
    private ComplaintStatusFlatRepository complaintStatusFlatRepository;

    public ComplaintStatus findByName(String name) {
        return complaintStatusFlatRepository.findByName(name);
    }
}
