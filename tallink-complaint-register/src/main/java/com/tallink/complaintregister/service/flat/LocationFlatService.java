package com.tallink.complaintregister.service.flat;

import com.tallink.complaintregister.dao.model.domain.flat.Location;
import com.tallink.complaintregister.dao.repository.flat.LocationFlatRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class LocationFlatService {

    @Autowired
    private LocationFlatRepository locationFlatRepository;

    public Location findByValue(String value) {
        return locationFlatRepository.findByValue(value);
    }
}
