package com.tallink.complaintregister.service;

import com.tallink.complaintregister.dao.model.domain.ComplaintItem;
import com.tallink.complaintregister.dao.repository.ComplaintItemRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class ComplaintItemService {

    @Autowired
    private ComplaintItemRepository complaintItemRepository;

    public Iterable<ComplaintItem> findAll() {
        return complaintItemRepository.findAll();
    }

    public Optional<ComplaintItem> findById(Integer id) {
        return complaintItemRepository.findById(id);
    };

    public List<ComplaintItem> findComplaintItemsByComplaintComplaintId(Integer complaintId) {
        return complaintItemRepository.findComplaintItemsByComplaintComplaintId(complaintId);
    }

    public ComplaintItem save(ComplaintItem complaintItem) {
        return complaintItemRepository.save(complaintItem);
    }
}
