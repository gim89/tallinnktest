package com.tallink.complaintregister.service.flat;

import com.tallink.complaintregister.dao.model.domain.flat.Supplier;
import com.tallink.complaintregister.dao.repository.flat.SupplierFlatRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Slf4j
@Service
public class SupplierFlatService {

    @Autowired
    private SupplierFlatRepository supplierFlatRepository;

    public Optional<Supplier> findSupplierFlatById(Integer id) {
        return supplierFlatRepository.findById(id);
    }

    public Supplier findSupplierFlatByNameAndRegistryNumberOrNull(String name, String registryNumber) {
        Supplier availableSupplier = null;

        try {
            availableSupplier = supplierFlatRepository.findFirstByNameAndRegistryNumber(name, registryNumber);
        } catch (Exception ex) {
            LOGGER.error("While trying to find supplier using name [" + name + "] and registry number [" + registryNumber + "]", ex);
        }

        return availableSupplier;
    }

    public Supplier save(Supplier supplierFlat) {
        return supplierFlatRepository.save(supplierFlat);
    }

}
