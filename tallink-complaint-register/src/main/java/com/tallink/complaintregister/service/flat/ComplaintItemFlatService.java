package com.tallink.complaintregister.service.flat;

import com.tallink.complaintregister.dao.model.domain.flat.ComplaintItem;
import com.tallink.complaintregister.dao.repository.flat.ComplaintItemFlatRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class ComplaintItemFlatService {

    @Autowired
    private ComplaintItemFlatRepository complaintItemFlatRepository;

    public Optional<ComplaintItem> findById(Integer id) {
        return complaintItemFlatRepository.findById(id);
    }

    public List<ComplaintItem> findComplaintItemsByComplaintId(Integer complaintId) {
        return complaintItemFlatRepository.findComplaintItemsByComplaintId(complaintId);
    }

    public ComplaintItem save(ComplaintItem complaintItemFlat) {
        return complaintItemFlatRepository.save(complaintItemFlat);
    }

    public void deleteByComplaintItemId(Integer id) {
        complaintItemFlatRepository.deleteById(id);
    }
}
