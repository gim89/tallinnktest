package com.tallink.complaintregister.service;

import com.tallink.complaintregister.dao.model.domain.ComplaintItemType;
import com.tallink.complaintregister.dao.repository.ComplaintItemTypeRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class ComplaintItemTypeService {

    @Autowired
    private ComplaintItemTypeRepository complaintItemTypeRepository;

    public Iterable<ComplaintItemType> findAll() {
        return complaintItemTypeRepository.findAll();
    }
}
