package com.tallink.complaintregister.service;

import com.tallink.complaintregister.dao.model.domain.ComplaintStatus;
import com.tallink.complaintregister.dao.repository.ComplaintStatusRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class ComplaintStatusService {

    @Autowired
    private ComplaintStatusRepository complaintStatusRepository;

    public Iterable<ComplaintStatus> findAllComplaintStatuses(){
        return complaintStatusRepository.findAll();
    }
}
