package com.tallink.complaintregister.mapping;

import com.tallink.complaintregister.dao.model.domain.Complaint;
import com.tallink.complaintregister.dao.model.domain.UserRole;
import com.tallink.complaintregister.web.api.DashboardComplaint;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.impl.ConfigurableMapper;
import org.springframework.stereotype.Component;

@Component
public class CustomMapper extends ConfigurableMapper {


    @Override
    public void configure(MapperFactory factory) {

        userRoleCustomMapper(factory);

        supplierFlatCustomMapper(factory);
        productFlatCustomMapper(factory);
        complaintItemFlatCustomMapper(factory);
        userFlatCustomMapper(factory);
        complaintFlatCustomMapper(factory);

        complaintCustomMapper(factory);
        complaintDashboardCustomMapper(factory);
        complaintItemCustomMapper(factory);
        complaintStatusCustomMapper(factory);
        complaintItemReasonTypeCustomMapper(factory);
        complaintItemDecisionTypeCustomMapper(factory);
        complaintItemTypeCustomMapper(factory);
        userCustomMapper(factory);
    }

    private void userCustomMapper(MapperFactory factory) {
        factory.classMap(com.tallink.complaintregister.web.api.auth.LoginResponse.class, com.tallink.complaintregister.web.api.User.class)
                .byDefault()
                .register();
    }

    private void complaintItemFlatCustomMapper(MapperFactory factory) {
        factory.classMap(com.tallink.complaintregister.dao.model.domain.flat.ComplaintItem.class, com.tallink.complaintregister.web.api.ComplaintItem.class)
                .field("bestBefore","bestBefore")
                .field("receivingDate","receivingDate")
                .field("productId","product.productId")
                .field("complaintItemTypeId","complaintItemType.complaintItemTypeId")
                .field("complaintItemReasonTypeId","complaintItemReasonType.complaintItemReasonTypeId")
                .field("complaintItemDecisionTypeId","complaintItemDecisionType.complaintItemDecisionTypeId")
                .byDefault()
                .register();
    }

    private void complaintItemCustomMapper(MapperFactory factory) {
        factory.classMap(com.tallink.complaintregister.dao.model.domain.ComplaintItem.class, com.tallink.complaintregister.web.api.ComplaintItem.class)
                .byDefault()
                .register();
    }

    private void complaintItemTypeCustomMapper(MapperFactory factory) {
        factory.classMap(com.tallink.complaintregister.dao.model.domain.ComplaintItemType.class, com.tallink.complaintregister.web.api.ComplaintItemType.class)
                .byDefault()
                .register();
    }

    private void complaintItemDecisionTypeCustomMapper(MapperFactory factory) {
        factory.classMap(com.tallink.complaintregister.dao.model.domain.ComplaintItemDecisionType.class, com.tallink.complaintregister.web.api.ComplaintItemDecisionType.class)
                .byDefault()
                .register();
    }

    private void complaintItemReasonTypeCustomMapper(MapperFactory factory) {
        factory.classMap(com.tallink.complaintregister.dao.model.domain.ComplaintItemReasonType.class, com.tallink.complaintregister.web.api.ComplaintItemReasonType.class)
                .byDefault()
                .register();
    }

    private void complaintStatusCustomMapper(MapperFactory factory) {
        factory.classMap(com.tallink.complaintregister.dao.model.domain.ComplaintStatus.class, com.tallink.complaintregister.web.api.ComplaintStatus.class)
                .byDefault()
                .register();
    }

    private void productFlatCustomMapper(MapperFactory factory) {
        factory.classMap(com.tallink.complaintregister.dao.model.domain.flat.Product.class, com.tallink.complaintregister.web.api.Product.class)
                .byDefault()
                .register();
    }

    private void supplierFlatCustomMapper(MapperFactory factory) {
        factory.classMap(com.tallink.complaintregister.dao.model.domain.flat.Supplier.class, com.tallink.complaintregister.web.api.Supplier.class)
                .byDefault()
                .register();
    }

    private void complaintCustomMapper(MapperFactory factory) {
        factory.classMap(Complaint.class, com.tallink.complaintregister.web.api.Complaint.class)
                .field("issueDate", "issueDate")
                .field("supplier.name", "supplier.name")
                .field("supplier.email", "supplier.email")
                .field("supplier.address", "supplier.address")
                .field("supplier.registryNumber", "supplier.registryNumber")
                .field("location.value", "location.value")
                .field("complaintStatus.name", "complaintStatus.name")
                .field("buyer.buyerId", "buyer.buyerId")
                .field("buyer.name", "buyer.name")
                .field("buyer.registryNumber", "buyer.registryNumber")
                .field("buyer.address", "buyer.address")
                .byDefault()
                .register();
    }

    private void complaintDashboardCustomMapper(MapperFactory factory) {
        factory.classMap(Complaint.class, DashboardComplaint.class)
                .field("issueDate", "issueDate")
                .field("user.username", "user")
                .field("location.value", "location")
                .field("complaintStatus.name", "complaintStatus")
                .field("supplier.name", "supplier")
                .byDefault()
                .customize(new ma.glasnost.orika.CustomMapper<Complaint, DashboardComplaint>() {
                    @Override
                    public void mapAtoB(Complaint complaint, DashboardComplaint dashboardComplaint, MappingContext context) {
                        Boolean foundCritical = false;
                        for (var complaintItem: complaint.getComplaintItems()) {
                            if (
                                complaintItem.getComplaintItemType() != null &&
                                complaintItem.getComplaintItemDecisionType() != null &&
                                complaintItem.getComplaintItemType().getComplaintItemTypeId() == 1 &&
                                complaintItem.getComplaintItemDecisionType().getComplaintItemDecisionTypeId() == 4
                            ) {
                                foundCritical = true;
                                break;
                            }
                        }
                        dashboardComplaint.setInvalidTypePresent(foundCritical);
                    }
                })
                .register();
    }

    private void userRoleCustomMapper(MapperFactory factory) {
        factory.classMap(UserRole.class, com.tallink.complaintregister.web.api.UserRole.class)
                .field("name", "role")
                .byDefault()
                .register();
    }

    private void userFlatCustomMapper(MapperFactory factory) {
        factory.classMap(com.tallink.complaintregister.dao.model.domain.flat.User.class, com.tallink.complaintregister.web.api.User.class)
                .field("locationId", "location.locationId")
                .field("userRoleId", "userRole.userRoleId")
                .byDefault()
                .register();
    }

    private void complaintFlatCustomMapper(MapperFactory factory) {
        factory.classMap(com.tallink.complaintregister.dao.model.domain.Complaint.class, com.tallink.complaintregister.dao.model.domain.flat.Complaint.class)
                .field("supplier.supplierId", "supplierId")
                .field("location.locationId","locationId")
                .field("complaintStatus.complaintStatusId", "complaintStatusId")
                .field("user.userId", "userId")
                .field("buyer.buyerId", "buyerId")
                .byDefault()
                .register();
    }
}
