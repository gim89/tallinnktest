package com.tallink.complaintregister.web.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Getter
@Setter
public class UserRole {

    @JsonProperty("userRoleId")
    private Integer userRoleId;

    @JsonProperty("role")
    private String role;
}
