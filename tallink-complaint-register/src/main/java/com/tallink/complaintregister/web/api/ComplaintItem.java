package com.tallink.complaintregister.web.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.time.ZonedDateTime;

@Data
@Getter
@Setter
public class ComplaintItem {

    @JsonProperty("complaintItemId")
    private Integer complaintItemId;

    @JsonProperty("name")
    private String name;

    @JsonProperty("bestBefore")
    private ZonedDateTime bestBefore;

    @JsonProperty("receivingDate")
    private ZonedDateTime receivingDate;

    @JsonProperty("product")
    private Product product;

    @JsonProperty("amount")
    private Double amount;

    @JsonProperty("price")
    private Double price;

    @JsonProperty("complaintItemType")
    private ComplaintItemType complaintItemType;

    @JsonProperty("complaintItemReasonType")
    private ComplaintItemReasonType complaintItemReasonType;

    @JsonProperty("complaintItemDecisionType")
    private ComplaintItemDecisionType complaintItemDecisionType;

    @JsonProperty("comment")
    private String comment;

    @JsonProperty("isHelpNeeded")
    private Boolean isHelpNeeded;
}
