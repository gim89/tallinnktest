package com.tallink.complaintregister.web.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Getter
@Setter
public class ComplaintItemReasonType {

    @JsonProperty("complaintItemReasonTypeId")
    private Integer complaintItemReasonTypeId;

    @JsonProperty("name")
    private String name;
}
