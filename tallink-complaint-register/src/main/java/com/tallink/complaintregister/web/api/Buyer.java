package com.tallink.complaintregister.web.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Getter
@Setter
public class Buyer {

    @JsonProperty("buyerId")
    private String buyerId;

    @JsonProperty("name")
    private String name;

    @JsonProperty("registryNumber")
    private String registryNumber;

    @JsonProperty("address")
    private String address;
}
