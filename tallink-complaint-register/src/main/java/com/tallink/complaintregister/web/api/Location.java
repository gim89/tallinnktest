package com.tallink.complaintregister.web.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Getter
@Setter
public class Location {

    @JsonProperty("locationId")
    private Integer locationId;

    @JsonProperty("value")
    private String value;
}
