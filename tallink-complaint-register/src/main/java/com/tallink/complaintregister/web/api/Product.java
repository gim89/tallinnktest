package com.tallink.complaintregister.web.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Getter
@Setter
public class Product {

    @JsonProperty("productId")
    private Integer productId;

    @JsonProperty("article")
    private String article;

    @JsonProperty("designation")
    private String designation;

    @JsonProperty("department")
    private String department;

    @JsonProperty("departmentOcmDesignation")
    private String departmentOcmDesignation;

    @JsonProperty("abbreviation")
    private String abbreviation;

    @JsonProperty("contents")
    private String contents;

    @JsonProperty("quantUnitOcmAbbreviation")
    private String quantUnitOcmAbbreviation;
}
