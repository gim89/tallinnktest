package com.tallink.complaintregister.web.api.auth;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
public class LoginResponse {
	@JsonProperty(value = "username")
	private String username;

	@JsonProperty(value = "fullName")
	private String fullName;

	@JsonProperty(value = "location")
	private String location;

	@JsonProperty(value = "token")
	private String token;
}
