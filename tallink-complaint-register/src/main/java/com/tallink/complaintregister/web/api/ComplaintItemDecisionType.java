package com.tallink.complaintregister.web.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Getter
@Setter
public class ComplaintItemDecisionType {

    @JsonProperty("complaintItemDecisionTypeId")
    private Integer complaintItemDecisionTypeId;

    @JsonProperty("name")
    private String name;
}
