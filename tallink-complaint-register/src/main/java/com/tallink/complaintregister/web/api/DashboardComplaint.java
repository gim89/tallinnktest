package com.tallink.complaintregister.web.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.time.ZonedDateTime;

@Data
@Getter
@Setter
public class DashboardComplaint {

    @JsonProperty("complaintId")
    private Integer complaintId;

    @JsonProperty("issueDate")
    private ZonedDateTime issueDate;

    @JsonProperty("location")
    private String location;

    @JsonProperty("complaintStatus")
    private String complaintStatus;

    @JsonProperty("user")
    private String user;

    @JsonProperty("supplier")
    private String supplier;

    @JsonProperty("invalidTypePresent")
    private Boolean invalidTypePresent;
}
