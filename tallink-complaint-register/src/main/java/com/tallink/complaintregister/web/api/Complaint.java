package com.tallink.complaintregister.web.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.time.ZonedDateTime;
import java.util.Set;

@Data
@Getter
@Setter
public class Complaint {

    @JsonProperty("complaintId")
    private Integer complaintId;

    @JsonProperty("issueDate")
    private ZonedDateTime issueDate;

    @JsonProperty("invoiceDate")
    private ZonedDateTime invoiceDate;

    @JsonProperty("referenceNumber")
    private String referenceNumber;

    @JsonProperty("supplier")
    private Supplier supplier;

    @JsonProperty("location")
    private Location location;

    @JsonProperty("complaintStatus")
    private ComplaintStatus complaintStatus;

    @JsonProperty("complaintItems")
    private Set<ComplaintItem> complaintItems;

    @JsonProperty("user")
    private User user;

    @JsonProperty("buyer")
    private Buyer buyer;
}
