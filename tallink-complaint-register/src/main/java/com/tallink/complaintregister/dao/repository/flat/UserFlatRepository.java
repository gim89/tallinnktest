package com.tallink.complaintregister.dao.repository.flat;

import com.tallink.complaintregister.dao.model.domain.flat.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import static com.tallink.complaintregister.ServiceNames.LOCAL_DATABASE_TRANSACTION_MANAGER;

@Repository
@Transactional(value = LOCAL_DATABASE_TRANSACTION_MANAGER)
public interface UserFlatRepository extends CrudRepository<User, Integer> {

    User findByUsername(String name);
}

