package com.tallink.complaintregister.dao.model.domain.flat;

import lombok.*;

import javax.persistence.*;
import java.time.ZonedDateTime;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "ComplaintItemFlat")
@Table(name = "complaint_item")
public class ComplaintItem {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "complaint_item_id")
    private Integer complaintItemId;

    @Column(name = "name")
    private String name;

    @Column(name = "best_before")
    private ZonedDateTime bestBefore;

    @Column(name = "receiving_date")
    private ZonedDateTime receivingDate;

    @Column(name = "complaint_id")
    private Integer complaintId;

    @Column(name = "product_id")
    private Integer productId;

    @Column(name = "amount")
    private Double amount;

    @Column(name = "price")
    private Double price;

    @Column(name = "complaint_item_type_id")
    private Integer complaintItemTypeId;

    @Column(name = "complaint_item_reason_type_id")
    private Integer complaintItemReasonTypeId;

    @Column(name = "complaint_item_decision_type_id")
    private Integer complaintItemDecisionTypeId;

    @Column(name = "comment", columnDefinition = "TEXT")
    private String comment;

    @Column(name = "is_help_needed")
    private Boolean isHelpNeeded;
}
