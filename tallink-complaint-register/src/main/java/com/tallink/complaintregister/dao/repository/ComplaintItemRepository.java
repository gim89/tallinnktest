package com.tallink.complaintregister.dao.repository;

import com.tallink.complaintregister.dao.model.domain.ComplaintItem;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static com.tallink.complaintregister.ServiceNames.LOCAL_DATABASE_TRANSACTION_MANAGER;

@Repository
@Transactional(value = LOCAL_DATABASE_TRANSACTION_MANAGER)
public interface ComplaintItemRepository extends CrudRepository<ComplaintItem, Integer> {

    List<ComplaintItem> findComplaintItemsByComplaintComplaintId(Integer complaintId);
}
