package com.tallink.complaintregister.dao.repository;

import com.tallink.complaintregister.dao.model.domain.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import static com.tallink.complaintregister.ServiceNames.LOCAL_DATABASE_TRANSACTION_MANAGER;

/**
 * Created by alekspo on 9/16/2019.
 */
@Repository
@Transactional(value = LOCAL_DATABASE_TRANSACTION_MANAGER)
public interface UserRepository extends CrudRepository<User, Integer> {
	User findUserByUsernameIgnoreCase(String name);
	boolean existsByUsernameIgnoreCase(String name);
}
