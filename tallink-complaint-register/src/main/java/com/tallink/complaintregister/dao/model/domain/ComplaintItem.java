package com.tallink.complaintregister.dao.model.domain;

import lombok.*;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.time.ZonedDateTime;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "ComplaintItem")
@Table(name = "complaint_item")
public class ComplaintItem {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "complaint_item_id")
    private Integer complaintItemId;

    @Column(name = "name")
    private String name;

    @Column(name = "best_before")
    private ZonedDateTime bestBefore;

    @Column(name = "receiving_date")
    private ZonedDateTime receivingDate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "complaint_id")
    private Complaint complaint;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "product_id", referencedColumnName = "product_id")
    @Fetch(value = FetchMode.JOIN)
    private Product product;

    @Column(name = "amount")
    private Double amount;

    @Column(name = "price")
    private Double price;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "complaint_item_type_id", referencedColumnName = "complaint_item_type_id")
    @Fetch(value = FetchMode.JOIN)
    private ComplaintItemType complaintItemType;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "complaint_item_reason_type_id", referencedColumnName = "complaint_item_reason_type_id")
    @Fetch(value = FetchMode.JOIN)
    private ComplaintItemReasonType complaintItemReasonType;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "complaint_item_decision_type_id", referencedColumnName = "complaint_item_decision_type_id")
    @Fetch(value = FetchMode.JOIN)
    private ComplaintItemDecisionType complaintItemDecisionType;

    @Column(name = "comment", columnDefinition = "TEXT")
    private String comment;

    @Column(name = "is_help_needed")
    private Boolean isHelpNeeded;
}
