package com.tallink.complaintregister.dao.model.domain.flat;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "ProductFlat")
@Table(name = "product")
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "product_id")
    private Integer productId;

    @Column(name = "article")
    private String article;

    @Column(name = "designation")
    private String designation;

    @Column(name = "department")
    private String department;

    @Column(name = "department_ocm_designation")
    private String departmentOcmDesignation;

    @Column(name = "abbreviation")
    private String abbreviation;

    @Column(name = "contents")
    private String contents;

    @Column(name = "quant_unit_ocm_abbreviation")
    private String quantUnitOcmAbbreviation;
}
