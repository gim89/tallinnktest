package com.tallink.complaintregister.dao.model.domain;

import lombok.*;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "Buyer")
@Table(name = "buyer")
public class Buyer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "buyer_id")
    private Integer buyerId;

    @Column(name = "name")
    private String name;

    @Column(name = "registry_number")
    private String registryNumber;

    @Column(name = "address")
    private String address;
}
