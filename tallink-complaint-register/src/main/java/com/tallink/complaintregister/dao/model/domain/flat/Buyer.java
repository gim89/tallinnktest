package com.tallink.complaintregister.dao.model.domain.flat;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "BuyerFlat")
@Table(name = "buyer")
public class Buyer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "buyer_id")
    private Integer buyerId;

    @Column(name = "name")
    private String name;

    @Column(name = "registry_number")
    private String registryNumber;

    @Column(name = "address")
    private String address;
}
