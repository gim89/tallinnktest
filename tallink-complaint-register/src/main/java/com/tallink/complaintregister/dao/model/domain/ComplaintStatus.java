package com.tallink.complaintregister.dao.model.domain;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "ComplaintStatus")
@Table(name = "complaint_status")
public class ComplaintStatus {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "complaint_status_id")
    private Integer complaintStatusId;

    @Column(name = "name")
    private String name;
}
