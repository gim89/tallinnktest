package com.tallink.complaintregister.dao.model.domain;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "ComplaintItemReasonType")
@Table(name = "complaint_item_reason_type")
public class ComplaintItemReasonType {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "complaint_item_reason_type_id")
    private Integer complaintItemReasonTypeId;

    @Column(name = "name")
    private String name;
}
