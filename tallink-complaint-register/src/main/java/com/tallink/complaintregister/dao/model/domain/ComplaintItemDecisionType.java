package com.tallink.complaintregister.dao.model.domain;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "ComplaintItemDecisionType")
@Table(name = "complaint_item_decision_type")
public class ComplaintItemDecisionType {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "complaint_item_decision_type_id")
    private Integer complaintItemDecisionTypeId;

    @Column(name = "name")
    private String name;
}
