package com.tallink.complaintregister.dao.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.tallink.complaintregister.dao.model.domain.Location;

import static com.tallink.complaintregister.ServiceNames.LOCAL_DATABASE_TRANSACTION_MANAGER;

@Repository
@Transactional(value = LOCAL_DATABASE_TRANSACTION_MANAGER)
public interface LocationRepository  extends CrudRepository<Location, Integer> {
    Location findLocationByLdapNameIgnoreCase(String ldapName);
}
