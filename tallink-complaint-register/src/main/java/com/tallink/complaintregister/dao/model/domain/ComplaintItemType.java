package com.tallink.complaintregister.dao.model.domain;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "ComplaintItemType")
@Table(name = "complaint_item_type")
public class ComplaintItemType {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "complaint_item_type_id")
    private Integer complaintItemTypeId;

    @Column(name = "name")
    private String name;
}
