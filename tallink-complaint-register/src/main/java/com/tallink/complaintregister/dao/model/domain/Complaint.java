package com.tallink.complaintregister.dao.model.domain;

import lombok.*;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.time.ZonedDateTime;
import java.util.Set;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "Complaint")
@Table(name = "complaint")
public class Complaint {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "complaint_id")
    private Integer complaintId;

    @Column(name = "issue_date")
    private ZonedDateTime issueDate;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "supplier_id", referencedColumnName = "supplier_id")
    @Fetch(value = FetchMode.JOIN)
    private Supplier supplier;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "location_id", referencedColumnName = "location_id")
    @Fetch(value = FetchMode.JOIN)
    private Location location;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "complaint_status_id", referencedColumnName = "complaint_status_id")
    @Fetch(value = FetchMode.JOIN)
    private ComplaintStatus complaintStatus;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id", referencedColumnName = "user_id")
    @Fetch(value = FetchMode.JOIN)
    private User user;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "buyer_id", referencedColumnName = "buyer_id")
    @Fetch(value = FetchMode.JOIN)
    private Buyer buyer;

    @OneToMany(
            mappedBy = "complaint",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private Set<ComplaintItem> complaintItems;

    @Column(name = "invoice_date")
    private ZonedDateTime invoiceDate;

    @Column(name = "reference_number")
    private String referenceNumber;
}
