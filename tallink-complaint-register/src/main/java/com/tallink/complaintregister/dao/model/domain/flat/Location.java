package com.tallink.complaintregister.dao.model.domain.flat;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "LocationFlat")
@Table(name = "location")
public class Location {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "location_id")
    private Integer locationId;

    @Column(name = "value")
    private String value;

    @Column(name = "ldap_name")
    private String ldapName;
}
