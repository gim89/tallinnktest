package com.tallink.complaintregister.dao.model.domain;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "ComplaintItemAsset")
@Table(name = "complaint_item_asset")
public class ComplaintItemAsset {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "complaint_item_asset_id")
    private Integer complaintItemAssetId;

    @Column(name = "complaint_item_id")
    private Integer complaintItemId;

    @Column(name = "value", columnDefinition = "BLOB")
    private String value;
}
