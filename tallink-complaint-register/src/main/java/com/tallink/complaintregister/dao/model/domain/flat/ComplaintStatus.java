package com.tallink.complaintregister.dao.model.domain.flat;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "ComplaintStatusFlat")
@Table(name = "complaint_status")
public class ComplaintStatus {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "complaint_status_id")
    private Integer complaintStatusId;

    @Column(name = "name")
    private String name;
}
