package com.tallink.complaintregister.dao.model.domain.flat;

import lombok.*;

import javax.persistence.*;
import java.time.ZonedDateTime;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "ComplaintFlat")
@Table(name = "complaint")
public class Complaint {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "complaint_id")
    private Integer complaintId;

    @Column(name = "supplier_id")
    private Integer supplierId;

    @Column(name = "location_id")
    private Integer locationId;

    @Column(name = "complaint_status_id")
    private Integer complaintStatusId;

    @Column(name = "user_id")
    private Integer userId;

    @Column(name = "buyer_id")
    private Integer buyerId;

    @Column(name = "issue_date")
    private ZonedDateTime issueDate;

    @Column(name = "invoice_date")
    private ZonedDateTime invoiceDate;

    @Column(name = "reference_number")
    private String referenceNumber;
}
