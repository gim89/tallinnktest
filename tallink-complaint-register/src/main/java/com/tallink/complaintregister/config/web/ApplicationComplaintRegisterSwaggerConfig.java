package com.tallink.complaintregister.config.web;

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.BasicAuth;
import springfox.documentation.service.SecurityScheme;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.Collections;

@Configuration
@EnableSwagger2
public class ApplicationComplaintRegisterSwaggerConfig {

	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2).
				apiInfo(applicationInformation()).
				useDefaultResponseMessages(false).
				select().
				apis(RequestHandlerSelectors.any()).
				paths(applicationDocumentedAPI()::apply).
				build().
				securitySchemes(Collections.singletonList(basicAuthenticationSecurityScheme()));
	}

	private Predicate<String> applicationDocumentedAPI() {
		return Predicates.or(
				PathSelectors.regex("/api/v1/auth.*"),
				PathSelectors.regex("/api/v1/auth/*"),
				PathSelectors.regex("/api/v1/complaint.*"),
				PathSelectors.regex("/api/v1/complaint/*"),
				PathSelectors.regex("/api/v1/complaint-status.*"),
				PathSelectors.regex("/api/v1/complaint-status/*"),
				PathSelectors.regex("/api/v1/product.*"),
				PathSelectors.regex("/api/v1/product/*")
		);
	}

	private SecurityScheme basicAuthenticationSecurityScheme() {
		return new BasicAuth("basicAuth");
	}

	private ApiInfo applicationInformation() {
		return new ApiInfo("Tallink Complaint Register API", null, null, null, null, null, null, new ArrayList<>());
	}
}
