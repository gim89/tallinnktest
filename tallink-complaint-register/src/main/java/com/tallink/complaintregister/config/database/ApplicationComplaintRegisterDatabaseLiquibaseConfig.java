package com.tallink.complaintregister.config.database;

import liquibase.configuration.GlobalConfiguration;
import liquibase.configuration.LiquibaseConfiguration;
import liquibase.integration.spring.SpringLiquibase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

import static com.tallink.complaintregister.ServiceNames.LOCAL_DATABASE_DATA_SOURCE;
import static com.tallink.complaintregister.ServiceNames.LOCAL_DATABASE_LIQUIBASE;

@Configuration
public class ApplicationComplaintRegisterDatabaseLiquibaseConfig {

    @Autowired
    @Qualifier(LOCAL_DATABASE_DATA_SOURCE)
    private DataSource localDatabaseDataSource;

    @Value("${cr.database.liquibase.should.run}")
    private boolean isLocalDatabaseLiquibaseShouldRun;

    @Value("${cr.database.liquibase.schema}")
    private String localDatabaseLiquibaseSchema;

    @Value("${cr.database.liquibase.change.log}")
    private String localDatabaseLiquibaseChangeLog;

    @Value("${cr.database.liquibase.change.log.table.name}")
    private String localDatabaseLiquibaseChangeLogTableName;

    @Value("${cr.database.liquibase.change.log.lock.table.name}")
    private String localDatabaseLiquibaseChangeLogLockTableName;

    @Bean(LOCAL_DATABASE_LIQUIBASE)
    public SpringLiquibase liquibase() {
        GlobalConfiguration liquibaseConfiguration = LiquibaseConfiguration.getInstance().getConfiguration(GlobalConfiguration.class);
        liquibaseConfiguration.setDatabaseChangeLogTableName(localDatabaseLiquibaseChangeLogTableName);
        liquibaseConfiguration.setDatabaseChangeLogLockTableName(localDatabaseLiquibaseChangeLogLockTableName);

        SpringLiquibase liquibase = new SpringLiquibase();
        liquibase.setDataSource(localDatabaseDataSource);
        liquibase.setShouldRun(isLocalDatabaseLiquibaseShouldRun);
        liquibase.setChangeLog(localDatabaseLiquibaseChangeLog);
        liquibase.setDefaultSchema(localDatabaseLiquibaseSchema);

        return liquibase;
    }
}