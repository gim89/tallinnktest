package com.tallink.complaintregister.config.security;

import com.tallink.complaintregister.dao.model.domain.Location;
import com.tallink.complaintregister.dao.model.domain.User;
import com.tallink.complaintregister.dao.repository.LocationRepository;
import com.tallink.complaintregister.dao.repository.RoleRepository;
import com.tallink.complaintregister.dao.repository.UserRepository;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ldap.core.DirContextOperations;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.ldap.userdetails.LdapUserDetailsImpl;
import org.springframework.security.ldap.userdetails.LdapUserDetailsMapper;

import java.util.Collection;

public class CustomUserLdapMapper extends LdapUserDetailsMapper {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private LocationRepository locationRepository;
    @Autowired
    private RoleRepository roleRepository;

    @Override
    public SecurityUserDetails mapUserFromContext(DirContextOperations ctx, String username, Collection<? extends GrantedAuthority> authorities){
        LdapUserDetailsImpl details = (LdapUserDetailsImpl)super.mapUserFromContext(ctx, username, authorities);
        // Example of getting attributes from Ldap
        //var allAttributes = ctx.getAttributes(); //All LDAP attributes
        //var unit = ctx.getStringAttribute("physicaldeliveryofficename");
        var department = ctx.getStringAttribute("department");
        Iterable<Location> locations = this.locationRepository.findAll();
        String[] distinguishedNames = StringUtils.split(details.getDn(), ",");
        var location = this.getLocation(distinguishedNames, locations);

        User user = null;
        if (userRepository.existsByUsernameIgnoreCase(username)){
            user = this.userRepository.findUserByUsernameIgnoreCase(username);
        }
        else{
            var fullname = ctx.getStringAttribute("displayname");
            user = new User();
            user.setFullName(fullname);
            user.setUsername(details.getUsername());
            if (location != null){
                user.setLocation(location);
            }
            user.setUserRole(this.roleRepository.findById(1).get());
            this.userRepository.save(user);
        }

        var securityUser = new SecurityUserDetails(user);
        securityUser.setDepartment(department);

        return securityUser;
    }

    public Location getLocation(String[] distinguishedNames, Iterable<Location> locations) {
        for (String distinguishedName : distinguishedNames) {
            String[] parameters = StringUtils.split(distinguishedName, "=");
            if (parameters[0].equalsIgnoreCase("ou")){
                for (Location location:locations) {
                    if (location.getLdapName().equalsIgnoreCase(parameters[1])){
                        return location;
                    }
                }
            }
        }
        return this.locationRepository.findLocationByLdapNameIgnoreCase("HQ");
    }
}
