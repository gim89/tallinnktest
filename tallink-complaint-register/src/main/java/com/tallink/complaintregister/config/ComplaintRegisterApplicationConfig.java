package com.tallink.complaintregister.config;

import com.tallink.complaintregister.config.database.ApplicationComplaintRegisterDatabaseConfig;
import com.tallink.complaintregister.config.database.ApplicationComplaintRegisterDatabaseLiquibaseConfig;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.liquibase.LiquibaseAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@EnableConfigurationProperties
@ComponentScan(basePackages = { "com.tallink.complaintregister" })
@EnableAutoConfiguration(exclude = {LiquibaseAutoConfiguration.class})
@Import({ ApplicationComplaintRegisterDatabaseLiquibaseConfig.class, ApplicationComplaintRegisterDatabaseConfig.class })
public class ComplaintRegisterApplicationConfig {

}
