package com.tallink.complaintregister.config.mail;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.soap.saaj.SaajSoapMessageFactory;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPException;

import static com.tallink.complaintregister.ServiceNames.CUSTOM_EMAIL_WEB_SERVICE_TEMPLATE;
import static com.tallink.complaintregister.ServiceNames.EMAIL_MARSHALLER;

@Configuration
public class ApplicationComplaintRegisterMailConfig {

	@Value("${cr.email.ws.url}")
	private String emailWsUrl;

	@Value("${cr.email.context.path}")
	private String emailContextPath;

	@Bean(EMAIL_MARSHALLER)
	public Jaxb2Marshaller emailMarshaller() {
		Jaxb2Marshaller emailMarshaller = new Jaxb2Marshaller();
		emailMarshaller.setContextPath(emailContextPath);

		return emailMarshaller;
	}

	@Bean(CUSTOM_EMAIL_WEB_SERVICE_TEMPLATE)
	public WebServiceTemplate customEmailWebServiceTemplate() throws SOAPException {
		WebServiceTemplate emailWebServiceTemplate = new WebServiceTemplate(new SaajSoapMessageFactory(MessageFactory.newInstance()));
		emailWebServiceTemplate.setMarshaller(emailMarshaller());
		emailWebServiceTemplate.setUnmarshaller(emailMarshaller());
		emailWebServiceTemplate.setDefaultUri(emailWsUrl);

		return emailWebServiceTemplate;
	}
}
