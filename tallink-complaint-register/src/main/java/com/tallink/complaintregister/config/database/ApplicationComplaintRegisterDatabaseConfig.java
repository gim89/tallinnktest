package com.tallink.complaintregister.config.database;

import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.Properties;

import static com.tallink.complaintregister.ServiceNames.*;

@Configuration
@EnableJpaRepositories(
        basePackages = "com.tallink.complaintregister.dao.repository",
        transactionManagerRef = LOCAL_DATABASE_TRANSACTION_MANAGER,
        entityManagerFactoryRef = LOCAL_DATABASE_ENTITY_MANAGER_FACTORY)
public class ApplicationComplaintRegisterDatabaseConfig {

    @Value("${cr.database.url}")
    private String localDatabaseUrl;
    @Value("${cr.database.password}")
    private String localDatabasePassword;
    @Value("${cr.database.username}")
    private String localDatabaseUsername;
    @Value("${cr.database.driver.class}")
    private String localDatabaseDriverClass;
    @Value("${cr.database.domain.package}")
    private String localDatabaseDomainPackage;
    @Value("${cr.database.hibernate.dialect}")
    private String localDatabaseHibernateDialect;
    @Value("${cr.database.hibernate.show.sql}")
    private String localDatabaseHibernateShowSQL;
    @Value("${cr.database.hibernate.hbm2.dll.auto}")
    private String localDatabaseHibernateHbm2DllAuto;
    @Value("${cr.database.hibernate.jdbc.time.zone}")
    private String localDatabaseHibernateJdbcTimeZone;

    @Bean(name = LOCAL_DATABASE_DATA_SOURCE)
    public DataSource localDatabaseDataSource() {
        HikariDataSource hikariDataSource = new HikariDataSource();
        hikariDataSource.setJdbcUrl(localDatabaseUrl);
        hikariDataSource.setUsername(localDatabaseUsername);
        hikariDataSource.setPassword(localDatabasePassword);
        hikariDataSource.setDriverClassName(localDatabaseDriverClass);

        return hikariDataSource;
    }

    @Bean(name = LOCAL_DATABASE_TRANSACTION_MANAGER)
    public PlatformTransactionManager localDatabaseTransactionManager(@Qualifier(LOCAL_DATABASE_ENTITY_MANAGER_FACTORY) EntityManagerFactory entityManagerFactory) {
        return new JpaTransactionManager(entityManagerFactory);
    }

    @Bean(name = LOCAL_DATABASE_ENTITY_MANAGER_FACTORY)
    @DependsOn(LOCAL_DATABASE_LIQUIBASE)
    public LocalContainerEntityManagerFactoryBean localDatabaseEntityManagerFactory() {
        LocalContainerEntityManagerFactoryBean entityManagerFactory = new LocalContainerEntityManagerFactoryBean();
        entityManagerFactory.setDataSource(localDatabaseDataSource());
        entityManagerFactory.setPackagesToScan(localDatabaseDomainPackage);

        JpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        entityManagerFactory.setJpaVendorAdapter(vendorAdapter);
        entityManagerFactory.setJpaProperties(localDatabaseHibernateProperties());

        return entityManagerFactory;
    }

    private Properties localDatabaseHibernateProperties() {
        Properties properties = new Properties();
        properties.setProperty("hibernate.jdbc.time_zone", localDatabaseHibernateJdbcTimeZone);
        properties.setProperty("hibernate.id.new_generator_mappings", "false");
        properties.setProperty("hibernate.hbm2ddl.auto", localDatabaseHibernateHbm2DllAuto);
        properties.setProperty("hibernate.show_sql", localDatabaseHibernateShowSQL);
        properties.setProperty("hibernate.dialect", localDatabaseHibernateDialect);
        properties.setProperty("hibernate.driver_class", localDatabaseHibernateDialect);

        return properties;
    }
}