package com.tallink.complaintregister.controller;

import com.tallink.complaintregister.mapping.CustomMapper;
import com.tallink.complaintregister.service.ComplaintService;
import com.tallink.complaintregister.service.EmailService;
import com.tallink.complaintregister.service.ExcelGenerationService;
import com.tallink.complaintregister.web.api.Complaint;
import com.tallink.complaintregister.web.api.ComplaintStatus;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;

@Slf4j
@RestController
@CrossOrigin
@RequestMapping("/api/v1/complaint")
@Api(
	value = "Tallink Complaint Register complaint operation(s)",
	description = "Tallink Complaint Register complaint operation(s) API",
	tags = {"Tallink Complaint Register complaint operation(s)"}
)
public class ComplaintController {

    @Autowired
    private ComplaintService complaintService;
    @Autowired
    private CustomMapper customMapper;
	@Autowired
	private ExcelGenerationService excelGenerationService;
	@Autowired
	private EmailService emailService;

	private HttpServletRequest request;

	@Value("${supplier.testing.email}")
	private String supplierTestingEmail;

	@Value("${supplier.email.subject}")
	private String supplierEmailSubject;

	@GetMapping("/test")
	public @ResponseBody
	String printTestString() {
		LOGGER.info(String.format("logger test v1 sent from %s ",getClientIp()));
		return "Hello from complaint backend service";
	}

    @GetMapping
    @ApiOperation(
		    value = "",
		    tags = {"Tallink Complaint Register complaint operation(s)"},
		    authorizations = {@Authorization(value = "basicAuth")}
    )
    public List<com.tallink.complaintregister.web.api.Complaint> getAllComplaints(@RequestParam(name = "status", required = false) String status) {

        if(!StringUtils.isEmpty(status))
            return customMapper.mapAsList(complaintService.findComplaintWithStatus(status), com.tallink.complaintregister.web.api.Complaint.class);

        return customMapper.mapAsList(complaintService.findAllComplaints(), com.tallink.complaintregister.web.api.Complaint.class);
    }

	@GetMapping("/dashboard")
	@ApiOperation(
			value = "",
			tags = {"Tallink Complaint Register complaint operation(s)"},
			authorizations = {@Authorization(value = "basicAuth")}
	)
	public List<com.tallink.complaintregister.web.api.DashboardComplaint> getAllDashboardComplaints(@RequestParam(name = "status", required = false) String status) {

		if(!StringUtils.isEmpty(status)){
		    Iterable<com.tallink.complaintregister.dao.model.domain.Complaint> complaintWithStatus = complaintService.findComplaintWithStatus(status);

			return customMapper.mapAsList(complaintWithStatus, com.tallink.complaintregister.web.api.DashboardComplaint.class);
		}

		return customMapper.mapAsList(complaintService.findAllComplaints(), com.tallink.complaintregister.web.api.DashboardComplaint.class);
	}

    @GetMapping("/{id}")
    @ApiOperation(
		    value = "",
		    tags = {"Tallink Complaint Register complaint operation(s)"},
		    authorizations = {@Authorization(value = "basicAuth")}
    )
    public com.tallink.complaintregister.web.api.Complaint getComplaintsByComplaintId(@PathVariable Integer id) {
        return customMapper.map(complaintService.findComplaintByIdOrNull(id), com.tallink.complaintregister.web.api.Complaint.class);
    }

	@PutMapping("/{complaintId}/update-status")
	@ApiOperation(
			value = "",
			tags = {"Tallink Complaint Register complaint operation(s)"},
			authorizations = {@Authorization(value = "basicAuth")}
	)
	public ComplaintStatus updateComplaintStatus(
			@PathVariable Integer complaintId,
			@RequestParam(name = "status") String status
	) throws IOException {
		if (status.equalsIgnoreCase("proceed")) {
			sendExcelComplaintByComplaintId(complaintId);
		}
		return customMapper.map(complaintService.updateComplaintStatusOrNull(complaintId, status), com.tallink.complaintregister.web.api.ComplaintStatus.class);
	}

	@GetMapping("/{id}/excel")
	@ApiOperation(
			value = "",
			tags = {"Tallink Complaint Register complaint operation(s)"},
			authorizations = {@Authorization(value = "basicAuth")}
	)
	public ResponseEntity<Resource> getExcelComplaintByComplaintId(@PathVariable Integer id) throws IOException {
		Complaint complaint = customMapper.map(complaintService.findComplaintByIdOrNull(id), Complaint.class);

		HSSFWorkbook hssfWorkbook = excelGenerationService.generateExcelForComplaint(complaint);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		hssfWorkbook.write(baos);
		ByteArrayResource byteArrayResource = new ByteArrayResource(baos.toByteArray());
		baos.close();

		return ResponseEntity.ok()
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=complaint_" + id + ".xls")
				.contentLength(byteArrayResource.contentLength())
				.contentType(MediaType.APPLICATION_OCTET_STREAM)
				.body(byteArrayResource);
	}

	@GetMapping("/{id}/excel/email")
	@ApiOperation(
			value = "",
			tags = {"Tallink Complaint Register complaint operation(s)"},
			authorizations = {@Authorization(value = "basicAuth")}
	)
	public ResponseEntity<Object> sendExcelComplaintByComplaintId(@PathVariable Integer id) throws IOException {
		Complaint complaint = customMapper.map(complaintService.findComplaintByIdOrNull(id), Complaint.class);

		if (!StringUtils.isEmpty(complaint.getSupplier().getEmail())) {
			HSSFWorkbook hssfWorkbook = excelGenerationService.generateExcelForComplaint(complaint);
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			hssfWorkbook.write(baos);

			// FIXME: replace with real e-mail (complaint.getSupplier().getEmail())
			String supplierEmail = (StringUtils.isEmpty(supplierTestingEmail)) ? complaint.getSupplier().getEmail() : supplierTestingEmail;
			String supplierSubject = (StringUtils.isEmpty(supplierEmailSubject)) ? supplierEmailSubject : "Complaint report" ;

			if (!StringUtils.isEmpty(supplierEmail)) {
				emailService.sendEmailWithAttachment(supplierEmail, supplierSubject,
						String.format("<p>This letter is to notify you concerning the product(s) that do not meet the expectations that we have been previously agreed upon. Please find enclosed the details about the product(s) in question and the specific issue description.</p>\n" +
										"<br /> \n" +
										"<p>Kind regards</p>\n" +
										"<br />\n" +
										"<p>This is a system-generated email; please do not respond to this email. Instead, use the contacts provided earlier.</p>\n" +
										"<br />\n" +
										"<br />\n" +
										"<br />\n" +
										"<p>Käesoleva kirjaga soovime juhtida teie tähelepanu tootele või toodetele, mis ei ole kooskõlas meie kokkuleppega. Leiate manusest detailse informatsiooni konkreetse toote ning seda puudutava puuduse kohta.</p>\n" +
										"<br />\n" +
										"<p>Parimate soovidega</p>\n" +
										"<br />\n" +
										"<p>See on süsteemi loodud e-kiri. Palun kasutage vastamiseks varasemaid kontaktandmeid.</p>"),
						"complaint_" + id + ".xls", baos.toByteArray()
				);
			}

			baos.close();
		}

		return ResponseEntity.ok().build();
	}

	@PostMapping("/create")
	public ResponseEntity<Complaint> createComplaint(@RequestBody Complaint complaint) {
		Complaint complaintResponse = complaintService.createComplaint(complaint);
		return new ResponseEntity<>(complaintResponse, HttpStatus.OK);
	}

	@PutMapping("/{id}")
	@ApiOperation(
			value = "",
			tags = {"Tallink Complaint Register complaint operation(s)"},
			authorizations = {@Authorization(value = "basicAuth")}
	)
    public ResponseEntity<Complaint> updateComplaint(@RequestBody com.tallink.complaintregister.web.api.Complaint complaint) throws IOException {
		com.tallink.complaintregister.dao.model.domain.Complaint complaintByIdOrNull = complaintService.findComplaintByIdOrNull(complaint.getComplaintId());
		if (complaintByIdOrNull != null) {
			if (!complaintByIdOrNull.getComplaintStatus().getName().equalsIgnoreCase(complaint.getComplaintStatus().getName())
					&& complaint.getComplaintStatus().getName().equalsIgnoreCase("proceed")) {
				sendExcelComplaintByComplaintId(complaint.getComplaintId());
			}
		}
		Complaint complaintResponse = complaintService.updateComplaint(complaint);
		return new ResponseEntity<>(complaintResponse, HttpStatus.OK);
    }

	private String getClientIp() {

		String remoteAddr = "";

		if (request != null) {
			remoteAddr = request.getHeader("X-FORWARDED-FOR");
			if (remoteAddr == null || "".equals(remoteAddr)) {
				remoteAddr = request.getRemoteAddr();
			}
		}

		return remoteAddr;
	}
}
