package com.tallink.complaintregister.controller;

import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tallink.complaintregister.dao.model.domain.User;
import com.tallink.complaintregister.dao.repository.ComplaintStatusRepository;
import com.tallink.complaintregister.dao.repository.UserRepository;
import com.tallink.complaintregister.mapping.CustomMapper;
import com.tallink.complaintregister.service.AuthenticationService;
import com.tallink.complaintregister.service.ComplaintService;
import com.tallink.complaintregister.web.api.Complaint;
import com.tallink.complaintregister.web.api.ComplaintStatus;
import com.tallink.complaintregister.web.api.auth.LoginResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@CrossOrigin
@RequestMapping("/api/v1/complaint-status")
@Api(value = "Tallink Complaint Register complaint status operation(s)", description = "Tallink Complaint Register complaint status operation(s) API", tags = {"Tallink Complaint Register complaint status operation(s)"})
public class ComplaintStatusController {

	@Autowired
	private ComplaintStatusRepository complaintStatusRepository;

	@Autowired
	private CustomMapper customMapper;

	@Autowired
	private ComplaintService complaintService;

	@Autowired
	private AuthenticationService authenticationService;

	@Autowired
	private UserRepository userRepository;

	@GetMapping("/all")
	@ApiOperation(
			value = "",
			tags = {"Tallink Complaint Register complaint status operation(s)"},
			authorizations = {@Authorization(value = "basicAuth")}
	)
	public List<com.tallink.complaintregister.web.api.ComplaintStatus> getAllComplaintStatuses() {
		return customMapper.mapAsList(complaintStatusRepository.findAll(), com.tallink.complaintregister.web.api.ComplaintStatus.class);
	}

	@GetMapping("/allowed/{complaintId}")
	public List<com.tallink.complaintregister.web.api.ComplaintStatus> getAllowedComplaintStatuses(@PathVariable Integer complaintId) {
		List<ComplaintStatus> complaintStatuses = customMapper.mapAsList(complaintStatusRepository.findAll(), ComplaintStatus.class);
		Complaint complaint = customMapper.map(complaintService.findComplaintByIdOrNull(complaintId), Complaint.class);
		LoginResponse userDetails = authenticationService.getAuthenticatedUserDetailsOrNull();

		User loggedInUser = userRepository.findUserByUsernameIgnoreCase(userDetails.getUsername());
		if (loggedInUser != null && complaint != null) {

			String complaintUsername = complaint.getUser().getUsername();
			if(!loggedInUser.getUsername().equals(complaintUsername)) {
				Iterator<ComplaintStatus> iter = complaintStatuses.iterator();
				while (iter.hasNext()) {
					ComplaintStatus complaintStatus = iter.next();
					if (!complaintStatus.getName().equalsIgnoreCase(complaint.getComplaintStatus().getName())) {
						iter.remove();
					}
				}
			}
		}

		return complaintStatuses;
	}
}
