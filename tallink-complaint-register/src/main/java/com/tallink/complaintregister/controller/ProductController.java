package com.tallink.complaintregister.controller;

import com.tallink.complaintregister.mapping.CustomMapper;
import com.tallink.complaintregister.service.ProductService;
import com.tallink.complaintregister.web.api.Product;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RestController
@CrossOrigin
@RequestMapping("/api/v1/product")
@Api(value = "Tallink Complaint Register product operation(s)", description = "Tallink Complaint Register product operation(s) API", tags = {"Tallink Complaint Register product operation(s)"})
public class ProductController {

	@Autowired
	private ProductService productService;

	@Autowired
	private CustomMapper customMapper;

	@GetMapping("/all")
	@ApiOperation(
			value = "",
			tags = {"Tallink Complaint Register product operation(s)"},
			authorizations = {@Authorization(value = "basicAuth")}
	)
	public List<Product> getAllComplaints() {
		return customMapper.mapAsList(productService.findAllProducts(), com.tallink.complaintregister.web.api.Product.class);
	}
}
