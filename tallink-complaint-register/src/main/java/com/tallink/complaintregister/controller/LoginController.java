package com.tallink.complaintregister.controller;

import com.tallink.complaintregister.config.security.JwtTokenProvider;
import com.tallink.complaintregister.web.api.auth.LoginRequest;
import com.tallink.complaintregister.web.api.auth.LoginResponse;
import com.tallink.complaintregister.web.api.auth.ValidateTokenRequest;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import javax.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@CrossOrigin
@RequestMapping("/api/v1/auth")
@Api(value = "Tallink Complaint Register authentication operation(s)", description = "Tallink Complaint Register authentication operation(s) API", tags = {"Tallink Complaint Register authentication operation(s)"})
public class LoginController {

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private JwtTokenProvider tokenProvider;

	@ApiOperation(
			value = "Generate payment token for order",
			tags = {"Tallink Complaint Register authentication operation(s)"},
			authorizations = {@Authorization(value = "basicAuth")}
	)
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<?> authenticate(@RequestBody LoginRequest loginRequest) {
		String decodePassword = new String(Base64.decodeBase64(loginRequest.getPassword()));
		if(loginRequest.getUsername().isEmpty() || loginRequest.getPassword().isEmpty()) {
			return new ResponseEntity<String>("Login/password is empty", HttpStatus.BAD_REQUEST);
		}
		Authentication authentication = null;
		try{
			authentication = authenticationManager.authenticate(
					new UsernamePasswordAuthenticationToken(
							loginRequest.getUsername(),
							decodePassword
					)
			);
		}catch (AuthenticationException ex){

		}

		if (authentication == null)
			return new ResponseEntity<String>("Wrong credentials", HttpStatus.UNAUTHORIZED);
		UserDetails userDetails = (UserDetails)authentication.getPrincipal();

		String jwt = tokenProvider.generateToken(authentication);
		LoginResponse user = LoginResponse.builder()
				.fullName("Test")
				.token(jwt)
				.location("TEST")
				.username(userDetails.getUsername())
				.build();

		return new ResponseEntity<LoginResponse>(user, HttpStatus.OK);
	}

	@PostMapping("/validatetoken")
	public ResponseEntity<?> getTokenByCredentials(@Valid @RequestBody ValidateTokenRequest validateToken) {
		String username = null;
		String jwt =validateToken.getToken();
		if (StringUtils.hasText(jwt) && tokenProvider.validateToken(jwt)) {
			return new ResponseEntity<Boolean>(Boolean.TRUE, HttpStatus.OK);
		}else {
			return new ResponseEntity<String>("Invalid Token", HttpStatus.BAD_REQUEST);
		}
	}
}
