package com.tallink.complaintregister.controller;

import com.tallink.complaintregister.mapping.CustomMapper;
import com.tallink.complaintregister.service.ComplaintItemService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RestController
@CrossOrigin
@RequestMapping("/api/v1/complaint-item")
public class ComplaintItemController {

    @Autowired
    private CustomMapper customMapper;

    @Autowired
    private ComplaintItemService complaintItemService;

    @GetMapping("/all")
    public List<com.tallink.complaintregister.web.api.ComplaintItem> getAllComplaintItems() {
        return customMapper.mapAsList(complaintItemService.findAll(), com.tallink.complaintregister.web.api.ComplaintItem.class);
    }
}
