package com.tallink.complaintregister.controller;

import com.tallink.complaintregister.mapping.CustomMapper;
import com.tallink.complaintregister.service.ComplaintItemReasonTypeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RestController
@CrossOrigin
@RequestMapping("/api/v1/complaint-item-reason-type")
public class ComplaintItemReasonType {

    @Autowired
    private CustomMapper customMapper;

    @Autowired
    private ComplaintItemReasonTypeService complaintItemReasonTypeService;

    @GetMapping("/all")
    public List<com.tallink.complaintregister.web.api.ComplaintItemReasonType> getAllComplaintStatuses() {
        return customMapper.mapAsList(complaintItemReasonTypeService.findAll(), com.tallink.complaintregister.web.api.ComplaintItemReasonType.class);
    }
}
