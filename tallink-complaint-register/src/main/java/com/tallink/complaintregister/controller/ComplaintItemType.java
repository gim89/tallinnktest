package com.tallink.complaintregister.controller;

import com.tallink.complaintregister.mapping.CustomMapper;
import com.tallink.complaintregister.service.ComplaintItemTypeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RestController
@CrossOrigin
@RequestMapping("/api/v1/complaint-item-type")
public class ComplaintItemType {

    @Autowired
    private CustomMapper customMapper;

    @Autowired
    private ComplaintItemTypeService complaintItemTypeService;

    @GetMapping("/all")
    public List<com.tallink.complaintregister.web.api.ComplaintItemType> getAllComplaintStatuses() {
        return customMapper.mapAsList(complaintItemTypeService.findAll(), com.tallink.complaintregister.web.api.ComplaintItemType.class);
    }
}
