package com.tallink.complaintregister.controller;

import com.tallink.complaintregister.mapping.CustomMapper;
import com.tallink.complaintregister.service.SupplierService;
import com.tallink.complaintregister.web.api.Supplier;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RestController
@CrossOrigin
@RequestMapping("/api/v1/supplier")
public class SupplierController {

    @Autowired
    private SupplierService supplierService;
    @Autowired
    private CustomMapper customMapper;

    @GetMapping("/all")
    public List<Supplier> getAllSuppliers() {
        return customMapper.mapAsList(supplierService.findAll(), com.tallink.complaintregister.web.api.Supplier.class);
    }
}
