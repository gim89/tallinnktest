import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ComplaintItem } from '../models/complaint.model';

@Injectable({
  providedIn: 'root'
})
export class ComplaintItemService {
  private headers: HttpHeaders;
  complaintId: number;
  private readonly endpoint: string = '/api/v1/complaint-item';
  constructor(private httpClient: HttpClient) { 
    this.headers = new HttpHeaders({ 'Content-Type': 'application/json; charset=utf-8' });
  }

  public updateComplaintItems(complaintId: number, complaintItems: ComplaintItem[]) {
    return this.httpClient.put(this.endpoint + "?complaintId=" + complaintId, complaintItems, { headers: this.headers }).subscribe( data  => {
          console.log("PUT Request is successful ", data);
        },
        error  => {
          console.log("Error", error);
        });
  }
}
