import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ComplaintStatus } from '../models/complaint.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ComplaintStatusService {
  private headers: HttpHeaders;
  private readonly endpoint: string = '/api/v1/complaint-status';
  constructor(private httpClient: HttpClient) {
    this.headers = new HttpHeaders({ 'Content-Type': 'application/json; charset=utf-8' });
  }

  public getAllComplaintStatuses(): Observable<ComplaintStatus[]> {
    return this.httpClient.get<ComplaintStatus[]>(this.endpoint + '/all', { headers: this.headers });
  }

  public getAllowedComplaintStatuses(complaintId: number): Observable<ComplaintStatus[]> {
    return this.httpClient.get<ComplaintStatus[]>(this.endpoint + '/allowed/' + complaintId, { headers: this.headers });
  }
}
