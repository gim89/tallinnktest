import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ComplaintItemReasonType } from '../models/complaint.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ComplaintItemReasonTypeService {
  private headers: HttpHeaders;
  private readonly endpoint: string = '/api/v1/complaint-item-reason-type/all';
  constructor(private httpClient: HttpClient) { 
    this.headers = new HttpHeaders({ 'Content-Type': 'application/json; charset=utf-8' });
  }

  public getAllComplaintItemReasonTypes(): Observable<ComplaintItemReasonType[]> {
    return this.httpClient.get<ComplaintItemReasonType[]>(this.endpoint, { headers: this.headers });
  }
}
