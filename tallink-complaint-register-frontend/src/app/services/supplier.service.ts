import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Supplier } from '../models/complaint.model';

@Injectable({
  providedIn: 'root'
})
export class SupplierService {
  private headers: HttpHeaders;
  complaintId: number;
  private readonly endpoint: string = '/api/v1/supplier';
  constructor(private httpClient: HttpClient) {
    this.headers = new HttpHeaders({ 'Content-Type': 'application/json; charset=utf-8' });
  }

  public getAllSuppliers(): Observable<Supplier[]>{
    return this.httpClient.get<Supplier[]>(this.endpoint + "/all", { headers: this.headers });
  }
}
