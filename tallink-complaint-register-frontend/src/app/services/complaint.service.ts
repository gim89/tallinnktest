import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Complaint, DashboardComplaint } from '../models/complaint.model';

@Injectable({
  providedIn: 'root'
})
export class ComplaintService {
  private headers: HttpHeaders;
  complaintId: number;
  private readonly endpoint: string = '/api/v1/complaint';
  constructor(private httpClient: HttpClient) {
    this.headers = new HttpHeaders({ 'Content-Type': 'application/json; charset=utf-8' });
  }

  public getComplaintDetailsById(complaintId: number): Observable<Complaint> {
    return this.httpClient.get<Complaint>(this.endpoint + '/' + complaintId, { headers: this.headers });
  }

  public getComplaintByComplaintStatus(complaintStatus: string): Observable<Complaint[]> {
    return this.httpClient.get<Complaint[]>(this.endpoint + '?status=' + complaintStatus, { headers: this.headers });
  }

  public getDashboardComplaintByComplaintStatus(complaintStatus: string): Observable<DashboardComplaint[]> {
    return this.httpClient.get<DashboardComplaint[]>(this.endpoint + '/dashboard?status=' + complaintStatus, { headers: this.headers });
  }

  public updateComplaint(complaintId: number, complaint: Complaint) {
    return this.httpClient.put(this.endpoint + '/' + complaintId, complaint , { headers: this.headers });
  }

  public createComplaint(complaint: Complaint) {
    return this.httpClient.post(this.endpoint + '/create', complaint, { headers: this.headers });
  }

  public updateComplaintStatus(complaintId: number, complaintStatus: string) {
    return this.httpClient.put(this.endpoint + '/' + complaintId + '/update-status?status=' + complaintStatus, { headers: this.headers });
  }
}
