import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ComplaintItemType } from '../models/complaint.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ComplaintItemTypeService {
  private headers: HttpHeaders;
  private readonly endpoint: string = '/api/v1/complaint-item-type/all';
  constructor(private httpClient: HttpClient) { 
    this.headers = new HttpHeaders({ 'Content-Type': 'application/json; charset=utf-8' });
  }

  public getAllComplaintItemTypes(): Observable<ComplaintItemType[]> {
    return this.httpClient.get<ComplaintItemType[]>(this.endpoint, { headers: this.headers });
  }
}
