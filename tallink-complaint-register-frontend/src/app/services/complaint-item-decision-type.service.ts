import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ComplaintItemDecisionType } from '../models/complaint.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ComplaintItemDecisionTypeService {
  private headers: HttpHeaders;
  private readonly endpoint: string = '/api/v1/complaint-item-decision-type/all';
  constructor(private httpClient: HttpClient) { 
    this.headers = new HttpHeaders({ 'Content-Type': 'application/json; charset=utf-8' });
  }

  public getAllComplaintItemDecisionTypes(): Observable<ComplaintItemDecisionType[]> {
    return this.httpClient.get<ComplaintItemDecisionType[]>(this.endpoint, { headers: this.headers });
  }
}
