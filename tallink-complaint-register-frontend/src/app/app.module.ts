import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { ComplaintMaterialModule } from './material/complaint-material-module';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ComplaintModule } from './components/pages/complaint/complaint.module';
import { FlexLayoutModule } from '@angular/flex-layout';

import { AppComponent } from './app.component';
import { AlertComponent } from './components/alert/alert.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { HeaderComponent } from './components/layout/header/header.component';
import { LoginComponent } from './components/pages/account/login/login.component';

import { AuthenticationService } from './services/data.service';

import { ErrorInterceptor } from './helper/error.interceptor';
import { JwtInterceptor } from './helper/jwt.interceptor';

import { AutoFocusDirective } from './directives/auto-focus.directive';
import { TestComponent } from './components/pages/test/test.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    AlertComponent,
    NotFoundComponent,
    LoginComponent,
    AutoFocusDirective,
    TestComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ComplaintMaterialModule,
    FormsModule,
    FlexLayoutModule,
    AppRoutingModule,
    HttpClientModule,
    ComplaintModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },

    AuthenticationService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
