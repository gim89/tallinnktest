export interface User {
    username: string;
    fullname: string;
    location: string;
    token?: string;
}
