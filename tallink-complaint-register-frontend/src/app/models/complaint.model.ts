export interface Supplier {
    name: string;
    email: string;
    address: string;
    registryNumber: string;
}

export interface Location {
    locationId: number;
    value: string;
}

export interface ComplaintStatus {
    name: string;
}

export interface Product {
    productId: number;
    article: string;
    designation: string;
    department: string;
    departmentOcmDesignation: string;
    abbreviation: string;
    contents: string;
    quantUnitOcmAbbreviation: string;
}

export interface ComplaintItemType {
    complaintItemTypeId: number;
    name: string;
}

export interface ComplaintItemReasonType {
    complaintItemReasonTypeId: number;
    name: string;
}

export interface ComplaintItemDecisionType {
    complaintItemDecisionTypeId: number;
    name: string;
}

export interface ComplaintItem {
    complaintItemId: number;
    name: string;
    bestBefore: Date;
    receivingDate: Date;
    product: Product;
    amount: number;
    price: number;
    complaintItemType: ComplaintItemType;
    complaintItemReasonType: ComplaintItemReasonType;
    complaintItemDecisionType: ComplaintItemDecisionType;
    comment: string;
    isHelpNeeded: boolean;
}

export interface UserRole {
    userRoleId: number;
    role: string;
}

export interface User {
    name: string;
    location: Location;
    userRole: UserRole;
}

export interface Buyer {
    buyerId: number;
    name: string;
    registryNumber: string;
    address: string;
}

export interface Complaint {
    complaintId: number;
    supplier: Supplier;
    location: Location;
    complaintStatus: ComplaintStatus;
    complaintItems: ComplaintItem[];
    user: User;
    issueDate: Date;
    invoiceDate: Date;
    referenceNumber: string;
    buyer: Buyer;
}

export interface DashboardComplaint {
    complaintId: number;
    location: string;
    complaintStatus: string;
    user: string;
    issueDate: Date;
    invalidTypePresent: boolean;
    supplier: string;
}
