import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ComplaintModule } from './components/pages/complaint/complaint.module'

import { NotFoundComponent } from './components/not-found/not-found.component';
import { LoginComponent } from './components/pages/account/login/login.component';
import { AuthGuard } from './helper/auth.guard';
import { ComplaintListComponent } from './components/pages/complaint/complaint-list/complaint-list.component';
import { TestComponent } from './components/pages/test/test.component';

const routes: Routes = [
  { path: '', component: ComplaintListComponent, canActivate: [AuthGuard] },
  { path: 'account/login', component: LoginComponent, pathMatch: 'full' },
  { path: 'test', component: TestComponent, pathMatch: 'full' },
  { path: '**', component: NotFoundComponent }
];

@NgModule({
  imports: [
    ComplaintModule,
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
