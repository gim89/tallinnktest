import { MatTableDataSource } from '@angular/material';
import { TableFilter } from './table-filter';

export class FilteredDataSource<T> extends MatTableDataSource<T> {
    private _filters: TableFilter[];

    set filters(filters: TableFilter[]) {
        this._filters = filters;
        this.filter = ' '; // Workaround to trigger filtering
    }

    /**
     * Filter predicate that will use _filters to filter.
     * This is a workaround as filterPredicate interface only allows filter to be a string.
     */
     filterPredicate = (data: T): boolean => {
        if (!this._filters || !this._filters.length) {
            return true;
        }

        const result = this._filters.reduce((visible, tableFilter) => {
            if (!visible) {
                return visible;
            }

            const filter = tableFilter.getFilter();

            return Object.keys(filter).reduce((show, columnName) => {
                if (!show) {
                    return show;
                }
                return this.matchesFilter(filter[columnName], data[columnName]);
            }, true);
        }, true);

        return result;
    }

    private matchesFilter(filterForColumn: any, dataForColumn: any): boolean {        

        if (filterForColumn.contains.toLowerCase() && dataForColumn.toLowerCase().indexOf(filterForColumn.contains.toLowerCase()) !== -1) {
            return true;
        }
        let time: Date = new Date(dataForColumn);
        if (filterForColumn.le && filterForColumn.ge) {                    
            if (time.getTime() >= filterForColumn.ge.getTime() && time.getTime() <= filterForColumn.le.getTime()) {
                return true;
            }
        } else if (filterForColumn.ge && time.getTime() >= filterForColumn.ge.getTime()) {
            return true;
        } else if (filterForColumn.le && time.getTime() <= filterForColumn.le.getTime()) {
            return true;
        }

        return false;
    }
}