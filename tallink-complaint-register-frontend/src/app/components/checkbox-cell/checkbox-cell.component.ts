import { AfterViewInit, Component, ViewChild, ElementRef } from '@angular/core';
import { ICellRendererParams } from 'ag-grid-community';
import { ICellRendererAngularComp } from 'ag-grid-angular';

@Component({
    selector: 'app-checkbox-cell',
    template: `<input type="checkbox" [checked]="params.value" (change)="onChange($event)">`,
    styleUrls: ['./checkbox-cell.component.css']
})
export class CheckboxCellComponent implements AfterViewInit, ICellRendererAngularComp {
    @ViewChild('.checkbox', { static: true }) checkbox: ElementRef;

    public params: ICellRendererParams;

    constructor() { }

    agInit(params: ICellRendererParams): void {
        this.params = params;
    }

    ngAfterViewInit(): void {
    }

    refresh(params: any): boolean {
        return true;
    }

    public onChange(event) {
        this.params.data[this.params.colDef.field] = event.currentTarget.checked;
    }
}
