import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../../services/data.service';
import { User } from 'src/app/models/user.model';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  currentUser: User;
  constructor(private router: Router, private dataService: AuthenticationService) {
    this.dataService.currentUser.subscribe(x => this.currentUser = x);
  }

  ngOnInit() { }

  logout(): void {
    this.dataService.logout();
    this.router.navigate(['/account/login']);
  }
}
