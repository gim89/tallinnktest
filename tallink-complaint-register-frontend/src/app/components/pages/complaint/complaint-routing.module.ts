import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ComplaintComponent } from './complaint.component';
import { ComplaintListComponent } from './complaint-list/complaint-list.component';
import { ComplaintDetailComponent } from './complaint-detail/complaint-detail.component';
import { ComplaintCreateComponent } from './complaint-create/complaint-create.component';

const routes: Routes = [
    {
      path: 'complaint-detail',
      component: ComplaintComponent,
      children: [
        {
          path: '',
          component: ComplaintListComponent,
        },
        {
          path: ':id',
          component: ComplaintDetailComponent
        }
      ]
    },
    {
      path: 'complaint-create',
      component: ComplaintComponent,
      children: [
        {
          path: '',
          component: ComplaintCreateComponent
        }
      ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ComplaintRoutingModule { }
