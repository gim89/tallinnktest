import { Component, OnInit, OnDestroy } from '@angular/core';
import { AutoCompleteComponent } from '../../../auto-complete/auto-complete.component';
import { ProductService } from 'src/app/services/product.service';
import { ComplaintService } from 'src/app/services/complaint.service';
import { ActivatedRoute } from '@angular/router';
import {
  Complaint,
  ComplaintItem,
  Product,
  ComplaintStatus,
  Location,
  ComplaintItemType,
  ComplaintItemDecisionType,
  ComplaintItemReasonType,
  Buyer,
  Supplier } from '../../../../models/complaint.model';
import { MatSnackBar } from '@angular/material';
import { Subscription } from 'rxjs';
import { GridOptions, GridApi, CellClassParams, RowNode } from 'ag-grid-community';
import { ComplaintStatusService } from 'src/app/services/complaint-status.service';
import { ComplaintItemTypeService } from 'src/app/services/complaint-item-type.service';
import { ComplaintItemDecisionTypeService } from 'src/app/services/complaint-item-decision-type.service';
import { ComplaintItemReasonTypeService } from 'src/app/services/complaint-item-reason-type.service';
import { ComplaintItemService } from 'src/app/services/complaint-item.service';
import { SupplierService } from 'src/app/services/supplier.service';
import { Router } from '@angular/router';
import 'jqueryui';
import * as moment from 'moment';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import { MomentDateModule, MomentDateAdapter } from '@angular/material-moment-adapter';
import {MatDatepicker} from '@angular/material/datepicker';
import { FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import {MatDatepickerInputEvent} from '@angular/material/datepicker';
import * as $ from 'jquery';
import {ErrorStateMatcher} from '@angular/material/core';

// See the Moment.js docs for the meaning of these formats:
// https://momentjs.com/docs/#/displaying/format/
export const MY_FORMATS = {
  parse: {
    dateInput: 'DD.MM.YYYY',
  },
  display: {
    dateInput: 'DD.MM.YYYY',
    monthYearLabel: 'MM YYYY',
    dateA11yLabel: 'DD.MM.YYYY',
    monthYearA11yLabel: 'MM YYYY',
  },
};

/** Error when invalid control is dirty, touched, or submitted. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-complaint-create',
  templateUrl: './complaint-create.component.html',
  styleUrls: ['./complaint-create.component.css'],
  providers: [
    // `MomentDateAdapter` can be automatically provided by importing `MomentDateModule` in your
    // application's root module. We provide it at the component level here, due to limitations of
    // our example generation script.
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
  ],
})
export class ComplaintCreateComponent implements OnInit, OnDestroy {

  constructor(
    private productService: ProductService,
    private complaintStatusService: ComplaintStatusService,
    private complaintItemTypeService: ComplaintItemTypeService,
    private complaintItemDecisionTypeService: ComplaintItemDecisionTypeService,
    private complaintItemReasonTypeService: ComplaintItemReasonTypeService,
    private complaintService: ComplaintService,
    private complaintItemService: ComplaintItemService,
    private supplierService: SupplierService,
    private route: ActivatedRoute,
    public snackBar: MatSnackBar,
    private router: Router
    ) {
      this.complaint.supplier = {} as Supplier;
      this.complaint.complaintStatus = {} as ComplaintStatus;
      this.complaint.complaintStatus.name = 'draft';
      this.complaint.location = {} as Location;
      this.complaint.complaintItems = [] as ComplaintItem[];
      this.frameworkComponents = {
        autoComplete: AutoCompleteComponent,
      };
      this.components = { datePicker: getDatePicker() };
      this.rowSelection = 'multiple';
  }
  private complaint: Complaint = {} as Complaint;
  private supplier = [];
  private suppliers: Supplier[];
  private filteredSuppliers: Supplier[];
  private frameworkComponents;
  private gridApi: GridApi;
  private rowData: ComplaintItem[];
  private rowSelection;
  private complaintDetailSubscription: Subscription;
  private complainStatuses: ComplaintStatus[];
  private components;
  events: string[] = [];
  minIssueDate = new Date(2000, 0, 1);
  minInvoiceDate = new Date(2000, 0, 1);

  date = new FormControl(moment());

  columnDefs = [
    { headerName: 'Article code', field: 'product.article', sortable: true, filter: true, editable: true },
    {
      headerName: 'Product name',
      field: 'name',
      sortable: true,
      filter: true,
      editable: true,
      cellEditor: 'autoComplete',
      cellEditorParams: {
        propertyRendered: 'designation',
        returnObject: false,
        rowData: [],
        columnDefs: [{ headerName: 'Item name', field: 'designation' }]
      },
      valueFormatter: (params) => {
        if (params.value) { return params.value.designation; }
        return '';
      }
    },
    {
      headerName: 'Comment',
      field: 'comment',
      sortable: true,
      editable: true,
      filter: 'agTextColumnFilter',
      maxLength: 2000,
      autoHeight: true,
      cellClass: 'cell-wrap-text',
      cellEditor: 'agLargeTextCellEditor',
      cellEditorParams: {
        maxLength: '2000',
        cols: '50',
        rows: '6'
    }
    },
    {
      headerName: 'Best before',
      field: 'bestBefore',
      sortable: true,
      filter: true,
      editable: true,
      type: 'dateColumn',
      cellEditor: 'datePicker',
      cellRenderer: this.dateRender
    },
    {
      headerName: 'Receiving date',
      field: 'receivingDate',
      sortable: true,
      filter: true,
      editable: true,
      type: 'dateColumn',
      cellEditor: 'datePicker',
      cellRenderer: this.dateRender
    },
    { headerName: 'Amount', field: 'amount', sortable: true, filter: true, editable: true,
    cellRenderer: (params) => {
      if (params.value) { return Number(params.value).toFixed(2); }
      return '';
    } },
    { headerName: 'Unit', field: 'product.abbreviation', sortable: true, filter: true, editable: true },
    { headerName: 'Price', field: 'price', sortable: true, filter: true, editable: true,
    cellRenderer: (params) => {
      if (params.value) { return Number(params.value).toFixed(2); }
      return '';
    } },
    {
      headerName: 'Sum',
      field: 'sum',
      sortable: true,
      filter: true,
      editable: false,
      valueGetter: '(data.price * data.amount).toFixed(2)'
    },
    {
      headerName: 'Type',
      field: 'complaintItemType',
      sortable: true,
      filter: true,
      editable: true,
      cellEditor: 'autoComplete',
      cellEditorParams: {
        propertyRendered: 'name',
        returnObject: true,
        rowData: [],
        columnDefs: [{ headerName: 'Types', field: 'name' }]
      },
      valueFormatter: (params) => {
        if (params.value) { return params.value.name; }
        return '';
      },
    },
    {
      headerName: 'Reason',
      field: 'complaintItemReasonType',
      sortable: true,
      filter: true,
      editable: true,
      cellEditor: 'autoComplete',
      cellEditorParams: {
        propertyRendered: 'name',
        returnObject: true,
        rowData: [],
        columnDefs: [{ headerName: 'Reason', field: 'name' }]
      },
      valueFormatter: (params) => {
        if (params.value) { return params.value.name; }
        return '';
      },
    },
    {
      headerName: 'Decision',
      field: 'complaintItemDecisionType',
      sortable: true,
      filter: true,
      editable: true,
      cellEditor: 'autoComplete',
      cellEditorParams: {
        propertyRendered: 'name',
        returnObject: true,
        rowData: [],
        columnDefs: [{ headerName: 'Decision', field: 'name' }]
      },
      valueFormatter: (params) => {
        if (params.value) { return params.value.name; }
        return '';
      },
    },
  ];
  defaultColDef: { width: 200, editable: true, filter: 'agTextColumnFilter' };

  supplierEmailFormControl = new FormControl('', [Validators.required]);
  referenceNumberFormControl = new FormControl('', [Validators.required]);
  invoiceDateFormControl = new FormControl('', [Validators.required]);
  supplierNameFormControl = new FormControl('', [Validators.required]);
  issueDateFormControl = new FormControl('', [Validators.required]);

  matcher = new MyErrorStateMatcher();

  dateRender(cell: CellClassParams): string {
    if (cell.value == null) { return ''; }
    const date: Date = cell.value as Date;
    return moment(date).format('DD.MM.YYYY');
  }

  chosenYearHandler(normalizedYear: moment.Moment) {
    const ctrlValue = this.date.value;
    ctrlValue.year(normalizedYear.year());
    this.date.setValue(ctrlValue);
  }

  chosenMonthHandler(normalizedMonth: moment.Moment, datepicker: MatDatepicker<moment.Moment>) {
    const ctrlValue = this.date.value;
    ctrlValue.month(normalizedMonth.month());
    this.date.setValue(ctrlValue);
    datepicker.close();
  }

  onGridReady(params: GridOptions): void {
    this.gridApi = params.api;
    this.gridApi.sizeColumnsToFit();
    setInterval(() => { this.gridApi.setPinnedBottomRowData([this.getPinnedRowData()]); });
  }

  ngOnDestroy(): void {
    this.complaintDetailSubscription.unsubscribe();
  }

  cellEditingStopped(event: CellClassParams): void {
    this.gridApi.setFocusedCell(event.rowIndex, event.colDef.field);
  }

  onCellValueChanged(item): void {
    item.newOrModified = 'Modified';

    if (this.complaint.complaintItems[item.rowIndex] != null &&
      this.columnDefs[1].cellEditorParams.rowData.find(
        o => o.designation === this.complaint.complaintItems[item.rowIndex].name
      ) != null
    ) {

      const product = this.complaint.complaintItems[item.rowIndex].product =
        this.columnDefs[1].cellEditorParams.rowData.find(
          o => o.designation === this.complaint.complaintItems[item.rowIndex].name
        );
      product.name = product.designation;
      this.gridApi.refreshCells();
      return product;
    }

    if (this.complaint.complaintItems[item.rowIndex] != null &&
       this.columnDefs[9].cellEditorParams.rowData.find(
        o => o.name === this.complaint.complaintItems[item.rowIndex].complaintItemType.name
        ) != null
      ) {

      this.complaint.complaintItems[item.rowIndex].complaintItemType.complaintItemTypeId =
      this.columnDefs[9].cellEditorParams.rowData.find(
        o => o.name === this.complaint.complaintItems[item.rowIndex].complaintItemType.name
        ).complaintItemTypeId;
    }

    if (this.complaint.complaintItems[item.rowIndex] != null &&
      this.columnDefs[10].cellEditorParams.rowData.find(
        o => o.name === this.complaint.complaintItems[item.rowIndex].complaintItemReasonType.name
        ) != null
      ) {

      this.complaint.complaintItems[item.rowIndex].complaintItemReasonType.complaintItemReasonTypeId =
      this.columnDefs[10].cellEditorParams.rowData.find(
        o => o.name === this.complaint.complaintItems[item.rowIndex].complaintItemReasonType.name
        ).complaintItemReasonTypeId;
    }

    if (this.complaint.complaintItems[item.rowIndex] != null &&
      this.columnDefs[11].cellEditorParams.rowData.find(
        o => o.name === this.complaint.complaintItems[item.rowIndex].complaintItemDecisionType.name
        ) != null
      ) {

      this.complaint.complaintItems[item.rowIndex].complaintItemDecisionType.complaintItemDecisionTypeId =
      this.columnDefs[11].cellEditorParams.rowData.find(
        o => o.name === this.complaint.complaintItems[item.rowIndex].complaintItemDecisionType.name
        ).complaintItemDecisionTypeId;
    }
  }

  getPinnedRowData(): {sum: number, price: number, amount: number} {
    const result = {
      sum: 0,
      price: 0,
      amount: 0
    };
    this.gridApi.forEachNodeAfterFilter((rowNode: RowNode) => {
      result.price += Number(rowNode.data.price);
      result.amount += Number(rowNode.data.amount);
    });
    return result;
  }

  getAllProducts(): void {
    this.productService.getAllProducts().subscribe((data: Product[]) => {
      this.columnDefs[1].cellEditorParams.rowData = data;
    });
  }
  getAllSuppliers(): void {
    this.supplierService.getAllSuppliers().subscribe((data: Supplier[]) => {
      this.suppliers = data;
      this.filteredSuppliers = data.sort((a, b) => {
          if (a.name < b.name) { return -1; }
          if (a.name > b.name) { return 1; }
          return 0;
      });
    });
  }
  getComplaintStatuses(): void {
    this.complaintDetailSubscription =
    this.complaintStatusService.getAllComplaintStatuses().subscribe((data: ComplaintStatus[]) => {
      this.complainStatuses = data;
    });
  }
  getComplaintItemTypes(): void {
    this.complaintItemTypeService.getAllComplaintItemTypes().subscribe((data: ComplaintItemType[]) => {
      this.columnDefs[9].cellEditorParams.rowData = data;
    });
  }
  getComplaintItemReasonTypes(): void {
    this.complaintItemReasonTypeService.getAllComplaintItemReasonTypes().subscribe((data: ComplaintItemReasonType[]) => {
      this.columnDefs[10].cellEditorParams.rowData = data;
    });
  }
  getComplaintItemDecisionTypes(): void {
    this.complaintItemDecisionTypeService.getAllComplaintItemDecisionTypes().subscribe((data: ComplaintItemDecisionType[]) => {
      this.columnDefs[11].cellEditorParams.rowData = data;
    });
  }

  ngOnInit(): void {
    this.getAllProducts();
    this.getComplaintStatuses();
    this.getComplaintItemReasonTypes();
    this.getComplaintItemDecisionTypes();
    this.getComplaintItemTypes();
    this.getAllSuppliers();
    this.rowData = [];
  }

  filterSuppliers(value: string) {
    if (value) {
      const filterValue = value.toLowerCase();

      return this.filteredSuppliers.filter(supplier => {
          if (supplier.name && supplier.name.toLowerCase().includes(filterValue)) {
            this.complaint.supplier.registryNumber = supplier.registryNumber;
            this.complaint.supplier.address = supplier.address;
            this.complaint.supplier.email = supplier.email;
            return supplier.name.toLowerCase().includes(filterValue);
          }
        });
    }
    this.complaint.supplier.name = '';
    this.complaint.supplier.registryNumber = '';
    this.complaint.supplier.address = '';
    return this.suppliers;
  }

  onAddRow() {
    const newItem = createNewRowData();
    const res = this.gridApi.updateRowData({ add: [newItem] });
    this.complaint.complaintItems.push(newItem);
  }

  onRemoveSelected() {
    const selectedData = this.gridApi.getSelectedRows();
    const res = this.gridApi.updateRowData({ remove: selectedData });

    for ( let i = 0; i < this.complaint.complaintItems.length; i++) {
      for ( let j = 0; j < selectedData.length; j++) {
        if ( selectedData[j] !== undefined
          && selectedData[j].name !== undefined
          && this.complaint.complaintItems[i].name === selectedData[j].name
          ) {
          this.complaint.complaintItems.splice(i, 1);
        }
      }
    }
  }

  invoiceDateChangeEvent(type: string, event: MatDatepickerInputEvent<Date>) {
    this.minIssueDate = new Date(event.value);
  }

  onClickSubmit(formData): void {

    if (this.referenceNumberFormControl.errors) {
      this.snackBar.open('Reference number may not be empty and not invalid!', '', { duration: 10000 });
      return;
    }
    if (this.invoiceDateFormControl.errors) {
      this.snackBar.open('Invoice date may not be empty and not invalid!', '', { duration: 10000 });
      return;
    }
    if (this.supplierNameFormControl.errors) {
      this.snackBar.open('Supplier name may not be empty and not invalid!', '', { duration: 10000 });
      return;
    }
    if (this.issueDateFormControl.errors) {
      this.snackBar.open('Issue date may not be empty and not invalid!', '', { duration: 10000 });
      return;
    }
    if (this.supplierEmailFormControl.errors) {
      this.snackBar.open('Supplier email may not be empty and not invalid!', '', { duration: 10000 });
      return;
    }
    if (new Date(this.invoiceDateFormControl.value) > new Date(this.issueDateFormControl.value)) {
      this.snackBar.open('Complaint "Invoice date" should not be smaller than "Complaint issue date"', '', { duration: 10000 });
      return;
    }

    let inValidComplaintItem = '';

    this.complaint.complaintItems.forEach( (item) => {
      if (item.name !== undefined || item.name !== '') {
        if ( item.complaintItemType.name === undefined || item.complaintItemType.name === '' ) {
          inValidComplaintItem = 'Type';
          return;
        }

        if ( item.complaintItemReasonType.name === undefined || item.complaintItemReasonType.name === '' ) {
          inValidComplaintItem = 'Reason';
          return;
        }

        if ( item.complaintItemDecisionType.name === undefined || item.complaintItemDecisionType.name === '' ) {
          inValidComplaintItem = 'Decision';
          return;
        }
      }

      inValidComplaintItem = '';
    });

    if ( inValidComplaintItem !== '' ) {
      this.snackBar.open('Complaint "' + inValidComplaintItem + '" should not be empty', '', { duration: 10000 });
      return;
    }

    if (formData.supplierAddress !== undefined && formData.supplierAddress.value !== '') {
      this.complaint.supplier.address = formData.supplierAddress;
    }
    if (formData.supplierName !== undefined && formData.supplierName.value !== '') {
      this.complaint.supplier.name = formData.supplierName;
    }
    if (formData.supplierRegistryNumber !== undefined && formData.supplierRegistryNumber.value !== '') {
      this.complaint.supplier.registryNumber = formData.supplierRegistryNumber;
    }
    if (formData.complaintStatus !== undefined && formData.complaintStatus.value !== '') {
      this.complaint.complaintStatus.name = formData.complaintStatus;
    }
    if (this.referenceNumberFormControl.value !== null) {
      this.complaint.referenceNumber = this.referenceNumberFormControl.value;
    }
    if (this.invoiceDateFormControl.value !== null) {
      this.complaint.invoiceDate = this.invoiceDateFormControl.value;
    }
    if (this.issueDateFormControl.value !== null) {
      this.complaint.issueDate = this.issueDateFormControl.value;
    }

    this.complaint.buyer = {} as Buyer;
    this.complaint.buyer.name = 'AS TALLINK GRUPP';
    this.complaint.buyer.registryNumber = '10238429';

    this.complaintService.createComplaint(this.complaint).subscribe( (data: Complaint)  => {
      console.log('POST Request is successful ', data);
      this.router.navigate(['/complaint-detail/' + data.complaintId]);
    }, error  => {
      console.log('Error', error);
    });

    this.snackBar.open('Complaint saved', '', {
      duration: 3000
    });
  }

}

let newCount = 1;
function createNewRowData() {
  const newComplaintDetailsItem = {
    complaintItemId: null,
    name: '',
    bestBefore: null,
    receivingDate: null,
    product: {
      productId: null,
      article: '',
      designation: '',
      department: '',
      departmentOcmDesignation: '',
      abbreviation: '',
      contents: '',
      quantUnitOcmAbbreviation: ''
    },
    amount: 0,
    price: 0,
    complaintItemType: {
      complaintItemTypeId: null,
      name: ''
    },
    complaintItemReasonType: {
      complaintItemReasonTypeId: null,
      name: ''
    },
    complaintItemDecisionType: {
      complaintItemDecisionTypeId: null,
      name: ''
    },
    comment: '',
    isHelpNeeded: false
  };
  newCount++;
  return newComplaintDetailsItem;
}
function getDatePicker() {
  function Datepicker() { }
  Datepicker.prototype.init = function(params) {
    this.eInput = document.createElement('input');
    this.eInput.value = params.value;
    $(this.eInput).datepicker({ dateFormat: 'dd.mm.yy', onSelect: (dateText: string, inst: any) => {
        const momentDate = moment(dateText, 'DD.MM.YYYY', true);
        this.eInput.value = momentDate.utc().format();
      } });
  };
  Datepicker.prototype.getGui = function() {
    return this.eInput;
  };
  Datepicker.prototype.afterGuiAttached = function() {
    this.eInput.focus();
    this.eInput.select();
  };
  Datepicker.prototype.getValue = function() {
    return this.eInput.value;
  };
  Datepicker.prototype.destroy = () => { };
  Datepicker.prototype.isPopup = () => {
    return false;
  };
  return Datepicker;
}