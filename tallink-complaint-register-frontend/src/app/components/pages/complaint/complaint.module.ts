// Module
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ComplaintRoutingModule } from './complaint-routing.module';
import { AgGridModule } from 'ag-grid-angular';
import { ComplaintMaterialModule } from './../../../material/complaint-material-module';

// Component
import { ComplaintComponent } from './complaint.component';
import { ComplaintListComponent } from './complaint-list/complaint-list.component';
import { ProceedListComponent } from './complaint-list/proceed/proceed-list.component';
import { ResolvedListComponent } from './complaint-list/resolved/resolved-list.component';
import { DraftListComponent } from './complaint-list/draft/draft-list.component';
import { AutoCompleteComponent } from '../../auto-complete/auto-complete.component';
import { ComplaintDetailComponent } from './complaint-detail/complaint-detail.component';
import { ComplaintCreateComponent } from './complaint-create/complaint-create.component';
import { TextFilterComponent } from '../../filter/filters/text-filter/text-filter.component';
import { DateFilterComponent } from '../../filter/filters/date-filter/date-filter.component';
import { TextCellComponent } from '../../filter/material-dynamic-table/table-cell/cell-types/text-cell.component';
import { DateCellComponent } from '../../filter/material-dynamic-table/table-cell/cell-types/date-cell.component';
import { DynamicTableComponent } from '../../filter/material-dynamic-table/dynamic-table.component';
import { TableCellComponent } from '../../filter/material-dynamic-table/table-cell/table-cell.component';
import { CheckboxCellComponent } from '../../checkbox-cell/checkbox-cell.component';

// Service
import { CellService } from '../../filter/material-dynamic-table/table-cell/cell-types/cell.service';
import { ColumnFilterService } from '../../filter/material-dynamic-table/table-cell/cell-types/column-filter.service';

// Directive
import { FilterItemDirective } from 'src/app/directives/filter-item.directive';
import { CellDirective } from '../../filter/material-dynamic-table/table-cell/cell.directive';

@NgModule({
    declarations: [
        ComplaintComponent,
        ComplaintListComponent,
        ComplaintDetailComponent,
        ProceedListComponent,
        ResolvedListComponent,
        DraftListComponent,
        AutoCompleteComponent,
        ComplaintCreateComponent,
        TextFilterComponent,
        DateFilterComponent,
        FilterItemDirective,
        DynamicTableComponent,
        TableCellComponent,
        CellDirective,
        TextCellComponent,
        DateCellComponent,
        CheckboxCellComponent
    ],
    imports: [
        ComplaintMaterialModule,
        FormsModule,
        ReactiveFormsModule,
        CommonModule,
        ComplaintRoutingModule,
        AgGridModule.withComponents([AutoCompleteComponent])
    ],
    entryComponents: [
        TextFilterComponent,
        DateFilterComponent,
        TextCellComponent,
        DateCellComponent,
        CheckboxCellComponent
    ],
    providers: [
        CellService,
        ColumnFilterService
    ],
    bootstrap: []
})
export class ComplaintModule {
    constructor(private readonly cellService: CellService, private readonly columnFilterService: ColumnFilterService) {
        columnFilterService.registerFilter('string', TextFilterComponent);
        columnFilterService.registerFilter('date', DateFilterComponent);

        cellService.registerCell('string', TextCellComponent);
        cellService.registerCell('date', DateCellComponent);
      }
 }
