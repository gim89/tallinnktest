import { Component, OnInit, ViewChild, Output, EventEmitter, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { ComplaintService } from 'src/app/services/complaint.service';
import { DashboardComplaint } from '../../../../../models/complaint.model';
import { DynamicTableComponent } from 'src/app/components/filter/material-dynamic-table/dynamic-table.component';
import { ColumnConfig } from 'src/app/components/filter/material-dynamic-table/column-config.model';
import { FilteredDataSource } from 'src/app/components/filter/data-source/filtered-data-source';
import { MatDialog } from '@angular/material';
import { ColumnFilterService } from 'src/app/components/filter/material-dynamic-table/table-cell/cell-types/column-filter.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-resolved-list',
  templateUrl: './resolved-list.component.html',
  styleUrls: ['./resolved-list.component.css']
})
export class ResolvedListComponent extends DynamicTableComponent implements OnInit, OnDestroy  {
  private complaintStatusSubscription: Subscription;
  private complaintEvent: Subscription;
  @ViewChild(DynamicTableComponent, { static: true }) dynamicTable: DynamicTableComponent;
  columns: ColumnConfig[] = [
    {
      name: 'complaintId',
      displayName: 'No.',
      type: ''
    },
    {
      name: 'location',
      displayName: 'Complaint Location',
      type: 'string'
    },
    {
      name: 'supplier',
      displayName: 'Supplier',
      type: 'string'
    },
    {
      name: 'issueDate',
      displayName: 'Date',
      type: 'date'
    }
  ];
  dataSource = new FilteredDataSource<DashboardComplaint>();

  @Output() complaintStatusUpdateOutput = new EventEmitter<string>();

  constructor(private readonly router: Router,
              private readonly columnFilter: ColumnFilterService,
              private readonly matdialog: MatDialog,
              private readonly complaintService: ComplaintService) {
    super(columnFilter, matdialog);
  }

  openComplaint(row: DashboardComplaint): void {
    this.router.navigate(['./complaint-detail', row.complaintId]);
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.displayedColumns.push('open');
    this.complaintStatusSubscription = this.complaintService.getDashboardComplaintByComplaintStatus('resolved')
    .subscribe((data: DashboardComplaint[]) => {
      this.dataSource.data = data;
    });
  }

  ngOnDestroy(): void {
    if (this.complaintStatusSubscription != null) {
      this.complaintStatusSubscription.unsubscribe();
    }
    if (this.complaintEvent != null) {
      this.complaintEvent.unsubscribe();
    }
  }

  updateComplaintStatusToResolved(event: Event, row: DashboardComplaint): void {
    event.stopPropagation();
    this.complaintEvent = this.complaintService.updateComplaintStatus(row.complaintId, 'proceed').subscribe(() => {
        this.complaintStatusUpdateOutput.emit('update-resolved-to-proceed-status');
    });
  }
}
