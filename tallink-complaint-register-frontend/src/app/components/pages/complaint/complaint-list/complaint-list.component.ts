import { Component, OnInit, ViewChild } from '@angular/core';
import { ResolvedListComponent } from './resolved/resolved-list.component';
import { ProceedListComponent } from './proceed/proceed-list.component';
import { DraftListComponent } from './draft/draft-list.component';

@Component({
  selector: 'app-complaint-list',
  templateUrl: './complaint-list.component.html',
  styleUrls: ['./complaint-list.component.css']
})
export class ComplaintListComponent implements OnInit {
  complaintStatus: string;

  @ViewChild(DraftListComponent, {static: true}) childDraftListComponent;
  @ViewChild(ResolvedListComponent, {static: true}) childResolvedListComponent;
  @ViewChild(ProceedListComponent, {static: true}) childProceedListComponent;

  ngOnInit() {
  }

  complaintStatusUpdated($event): void {
    this.complaintStatus = $event;

    if(this.complaintStatus === 'update-proceed-to-resolved-status' || this.complaintStatus === 'update-resolved-to-proceed-status') {
      this.childResolvedListComponent.ngOnInit();
      this.childProceedListComponent.ngOnInit();
    } 
    if(this.complaintStatus === 'update-draft-to-proceed-status') {
      this.childDraftListComponent.ngOnInit();
      this.childProceedListComponent.ngOnInit();
    }
  }
}
