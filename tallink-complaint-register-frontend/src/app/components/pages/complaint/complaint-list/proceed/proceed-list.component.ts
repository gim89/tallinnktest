import { Component, OnInit, ViewChild, Output, EventEmitter, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { ComplaintService } from 'src/app/services/complaint.service';
import { DashboardComplaint } from '../../../../../models/complaint.model';
import { DynamicTableComponent } from 'src/app/components/filter/material-dynamic-table/dynamic-table.component';
import { ColumnConfig } from 'src/app/components/filter/material-dynamic-table/column-config.model';
import { FilteredDataSource } from 'src/app/components/filter/data-source/filtered-data-source';
import { MatDialog } from '@angular/material';
import { ColumnFilterService } from 'src/app/components/filter/material-dynamic-table/table-cell/cell-types/column-filter.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-proceed-list',
  templateUrl: './proceed-list.component.html',
  styleUrls: ['./proceed-list.component.css']
})
export class ProceedListComponent extends DynamicTableComponent implements OnInit, OnDestroy {
  private complaintStatusSubscription: Subscription;
  private complaintEvent: Subscription;
  @ViewChild(DynamicTableComponent, { static: true }) dynamicTable: DynamicTableComponent;
  columns: ColumnConfig[] = [
    {
      name: 'complaintId',
      displayName: 'No.',
      type: ''
    },
    {
      name: 'location',
      displayName: 'Complaint Location',
      type: 'string'
    },
    {
      name: 'supplier',
      displayName: 'Supplier',
      type: 'string'
    },
    {
      name: 'issueDate',
      displayName: 'Date',
      type: 'date'
    }
  ];
  dataSource = new FilteredDataSource<DashboardComplaint>();
  private daysUntilComplaintIssueDateIsInValid = -7;

  @Output() complaintStatusUpdateOutput = new EventEmitter<string>();

  constructor(private readonly router: Router,
              private readonly columnFilter: ColumnFilterService,
              private readonly matdialog: MatDialog,
              private readonly complaintService: ComplaintService) {
    super(columnFilter, matdialog);
  }

  openComplaint(row: DashboardComplaint): void {
    this.router.navigate(['./complaint-detail', row.complaintId]);
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.displayedColumns.push('solve');
    this.complaintStatusSubscription = this.complaintService.getDashboardComplaintByComplaintStatus('proceed')
    .subscribe((data: DashboardComplaint[]) => {
      this.dataSource.data = data;
    });
  }

  ngOnDestroy(): void {
    if (this.complaintStatusSubscription != null) {
      this.complaintStatusSubscription.unsubscribe();
    }
    if (this.complaintEvent != null) {
      this.complaintEvent.unsubscribe();
    }
  }

  doesComplaintHaveNotAllowedComplaintItemType(complaint: DashboardComplaint): string {
    let isInValid = '';
    if (complaint.invalidTypePresent) { isInValid = 'not-valid-complaint-item-type-present'; }
    return isInValid;
  }

  updateComplaintStatusToResolved(event: Event, row: DashboardComplaint): void {
    event.stopPropagation();
    this.complaintEvent = this.complaintService.updateComplaintStatus(row.complaintId, 'resolved').subscribe(() => {
        this.complaintStatusUpdateOutput.emit('update-proceed-to-resolved-status');
    });
  }

  addClassIfComplaintIssueDateIsPassed(date: Date): string {
    const match = new Date(date).getTime(); // convert date to number
    const today = new Date().setHours(0, 0, 0, 0); // get present day as number
    const day = (n) => today + (86400000 * n); // assuming all days are 86400000 milliseconds, then add or remove days from today

    if (match <= day(this.daysUntilComplaintIssueDateIsInValid)) {
      return 'not-valid-date';
    }
 }
}
