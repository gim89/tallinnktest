import { Component, Renderer, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthenticationService } from 'src/app/services/data.service';
import { AlertService } from 'src/app/services/alert.service';
import { HttpErrorResponse } from '@angular/common/http';
import { first } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { User } from 'src/app/models/user.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [AlertService]
})
export class LoginComponent implements OnInit, OnDestroy {
  private dateServiceSubscription: Subscription;
  username: string;
  password: string;
  returnUrl: string;

  constructor(private router: Router,
              private route: ActivatedRoute,
              private renderer: Renderer,
              private alertService: AlertService,
              private dataService: AuthenticationService) {
                if (this.dataService.currentUserValue) {
                  this.router.navigate(['/']);
              }
  }

  ngOnInit(): void {
    this.renderer.setElementClass(document.body, 'overlay', true);
    this.returnUrl = this.route.snapshot.queryParams[`returnUrl`] || '/';
  }

  ngOnDestroy(): void {
    this.renderer.setElementClass(document.body, 'overlay', false);
    this.dateServiceSubscription.unsubscribe();
  }

  login(): void {
    this.dateServiceSubscription = this.dataService.login(this.username, this.password).pipe(first()).subscribe(
      (data: User) => {
        this.router.navigate([this.returnUrl]);
      },
      (error: HttpErrorResponse) => {
        this.alertService.error(`Server error: ${error}`);
      }
    );
  }
}
